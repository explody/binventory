#########################
#                       #
#   Required settings   #
#                       #
#########################
import os

# Don't change this or a black hole will form
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

# This is a list of valid fully-qualified domain names (FQDNs) for the Argus server.
# Argus will not permit write access to the server via any other hostnames. The first
# FQDN in the list will be treated as the preferred name.
# Example: ALLOWED_HOSTS = ['argus.example.com', 'argus.internal.local']
ALLOWED_HOSTS = []

# Database configuration.
DATABASE = {
    "ENGINE": "django.db.backends.sqlite3",
    "NAME": os.path.join(BASE_DIR, "db.sqlite3"),  # Database name
    "USER": "",  # Databse username, if relevant
    "PASSWORD": "",  # Database password, if relevant
    "HOST": "",  # Database server
    "PORT": "",  # Database port (leave blank for default)
}

# This key is used for secure generation of random numbers and strings. It must never be
# exposed outside of this file. For optimal security, SECRET_KEY should be at least 50
# characters in length and contain  a mix of letters, numbers, and symbols. Argus will
# not run without this defined. For more information, see
# https://docs.djangoproject.com/en/dev/ref/settings/#std:setting-SECRET_KEY
SECRET_KEY = ""

# Google API key for maps
GOOGLE_API_KEY = ""

#########################
#                       #
#   Optional settings   #
#                       #
#########################

# Specify one or more name and email address tuples representing Argus administrators.
# These people will be notified of application errors (assuming correct email
# settings are provided).
ADMINS = [
    # ['John Doe', 'jdoe@example.com'],
]

CONTACT_EMAIL = "admin@domain.com"

# Optionally display a persistent banner at the top and/or bottom of every page. HTML
# is allowed. To display the same content in both banners, define BANNER_TOP and set
# BANNER_BOTTOM = BANNER_TOP.
BANNER_TOP = ""
BANNER_BOTTOM = ""

# Text to include on the login page above the login form. HTML is allowed.
BANNER_LOGIN = ""

# Base URL path if accessing Argus within a directory. For example, if installed at
# http://example.com/argus/, set:
# BASE_PATH = 'argus/'
BASE_PATH = ""

# Maximum number of days to retain logged changes. Set to 0 to retain changes
# indefinitely. (Default: 90)
CHANGELOG_RETENTION = 90

# API Cross-Origin Resource Sharing (CORS) settings. If CORS_ORIGIN_ALLOW_ALL is set to
# True, all origins will be allowed. Otherwise, define a list of allowed origins using
# either CORS_ORIGIN_WHITELIST or CORS_ORIGIN_REGEX_WHITELIST. For more information,
# see https://github.com/ottoyiu/django-cors-headers
CORS_ORIGIN_ALLOW_ALL = False
CORS_ORIGIN_WHITELIST = [
    # 'hostname.example.com',
]
CORS_ORIGIN_REGEX_WHITELIST = [
    # r'^(https?://)?(\w+\.)?example\.com$',
]

# Set to True to enable server debugging. WARNING: Debugging introduces a substantial
# performance penalty and may reveal sensitive information about your installation.
# Only enable debugging while performing testing. Never enable debugging on a production
# system.
DEBUG = False

# Email settings
EMAIL = {
    "SERVER": "localhost",
    "PORT": 25,
    "USERNAME": "",
    "PASSWORD": "",
    "TIMEOUT": 10,  # seconds
    "FROM_EMAIL": "",
}

# Enable custom logging. Please see the Django documentation for detailed guidance on
# configuring custom logs:
#   https://docs.djangoproject.com/en/1.11/topics/logging/
LOGGING = {}

# Setting this to True will display a "maintenance mode" banner at the top of every page.
MAINTENANCE_MODE = False

# An API consumer can request an arbitrary number of objects =by appending the "limit"
# parameter to the URL (e.g. "?limit=1000"). This setting defines the maximum limit.
# Setting it to 0 or None will allow an API consumer to request all objects by
# specifying "?limit=0".
MAX_PAGE_SIZE = 10000

# The file path where uploaded media such as image attachments are stored. A trailing
# slash is not needed. Note that the default value of this setting is derived from
# the installed location.
# MEDIA_ROOT = '/srv/argus/static/media'


# Determine how many objects to display per page within a list. (Default: 50)
PAGINATE_COUNT = 50

# The Webhook event backend is disabled by default. Set this to True to enable it.
# Note that this requires a Redis database be configured and accessible by Argus
# (see `REDIS` below).
WEBHOOKS_ENABLED = False

# Redis database settings (optional). A Redis database is required only if the webhooks
# backend is enabled.
REDIS = {
    "HOST": "localhost",
    "PORT": 6379,
    "PASSWORD": "",
    "DATABASE": 0,
    "DEFAULT_TIMEOUT": 300,
}

# Time zone (default: UTC)
TIME_ZONE = "UTC"

# Date/time formatting. See the following link for supported formats:
# https://docs.djangoproject.com/en/dev/ref/templates/builtins/#date
DATE_FORMAT = "N j, Y"
SHORT_DATE_FORMAT = "Y-m-d"
TIME_FORMAT = "g:i a"
SHORT_TIME_FORMAT = "H:i:s"
DATETIME_FORMAT = "N j, Y g:i a"
SHORT_DATETIME_FORMAT = "Y-m-d H:i"

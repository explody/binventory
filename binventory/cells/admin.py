from django.contrib import admin
from django import forms
from .models import CellModel, Cell, CellTest, CellManufacturer


class CellModelAdminForm(forms.ModelForm):
    class Meta:
        model = CellModel
        fields = [
            "name",
            "form_factor",
            "manufacturer",
            "nominal_capacity",
            "minimum_capacity",
            "nominal_voltage",
            "charging_voltage",
            "cutoff_voltage",
            "max_charge_current",
            "max_discharge_current",
            "peak_discharge_current",
            "minimum_temperature",
            "maximum_temperature",
            "weight",
            "height",
            "width",
            "depth",
            "diameter",
            "description",
        ]
        validators = []


@admin.register(CellModel)
class CellModelAdmin(admin.ModelAdmin):
    form = CellModelAdminForm
    list_display = [
        "name",
        "form_factor",
        "manufacturer",
    ]
    readonly_fields = ["created", "last_updated"]
    search_fields = ["name", "form_factor", "manufacturer"]


class CellAdminForm(forms.ModelForm):
    class Meta:
        model = Cell
        fields = [
            "serial_number",
            "model",
        ]


@admin.register(Cell)
class CellAdmin(admin.ModelAdmin):
    form = CellAdminForm
    list_display = [
        "serial_number",
        "model",
    ]
    readonly_fields = ["created", "last_updated"]


class CellManufacturerAdminForm(forms.ModelForm):
    class Meta:
        model = CellManufacturer
        fields = ["name"]


@admin.register(CellManufacturer)
class CellManufacturerAdmin(admin.ModelAdmin):
    form = CellManufacturerAdminForm
    list_display = [
        "name",
    ]
    readonly_fields = ["created", "last_updated"]


class CellTestAdminForm(forms.ModelForm):
    class Meta:
        model = CellTest
        fields = [
            "cell",
            "elapsed_time",
            "high_voltage",
            "low_voltage",
            "discharge_rate",
            "calculated_capacity",
        ]


@admin.register(CellTest)
class CellTestAdmin(admin.ModelAdmin):
    form = CellTestAdminForm
    list_display = [
        "cell",
        "elapsed_time",
        "high_voltage",
        "low_voltage",
        "discharge_rate",
        "calculated_capacity",
    ]
    readonly_fields = ["created", "last_updated"]

from core.constants import *
from django.contrib.contenttypes.fields import GenericRelation
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import (
    CharField,
    FloatField,
    IntegerField,
    PositiveIntegerField,
    TextField,
)
from django.utils.translation import gettext as _
from django_extensions.db.fields import AutoSlugField
from library.models import BaseBattery, BaseManufacturer, BaseModel, ChangeLoggedModel
from library.utils import od_to_choices


class CellManufacturer(BaseManufacturer, ChangeLoggedModel):
    class Meta:
        verbose_name_plural = _("Cell Manufacturers")


class CellModel(BaseBattery, ChangeLoggedModel):
    class Meta:
        verbose_name_plural = _("Cell Models")

    slug = AutoSlugField(
        populate_from=["manufacturer", "name", "form_factor"], blank=True
    )
    name = CharField(_("Model Name"), max_length=255, blank=True, null=True)
    model_number = CharField(_("Model Number"), max_length=255, blank=True, null=True)
    form_factor = IntegerField(choices=od_to_choices(FORM_FACTOR))
    chemistry = IntegerField(choices=od_to_choices(BATTERY_CHEM))
    manufacturer = models.ForeignKey(to=CellManufacturer, on_delete=models.PROTECT)


class Cell(BaseModel, ChangeLoggedModel):
    # Fields
    slug = AutoSlugField(
        populate_from=["model__manufacturer", "serial_number"], blank=True
    )
    serial_number = CharField(_("Serial Number"), max_length=255)
    model = models.ForeignKey(to=CellModel, on_delete=models.PROTECT)
    color = IntegerField(choices=od_to_choices(COLORS))

    pack = models.ForeignKey("packs.Pack", on_delete=models.PROTECT)

    class Meta:
        verbose_name_plural = _("Cells")


class CellTest(BaseModel):
    cell = models.ForeignKey(to=Cell, on_delete=models.CASCADE)

    elapsed_time = PositiveIntegerField(_("Elapsed Time"))
    high_voltage = FloatField(_("High Voltage"))
    low_voltage = FloatField(_("Low Voltage"))
    discharge_rate = FloatField(_("Discharge Rate"))
    calculated_capacity = FloatField(_("Caulculated Capacity"))

    class Meta:
        verbose_name_plural = _("Cell Tests")

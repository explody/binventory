from library.serializers import UpdatableSlugSerializer, WritableIDRelatedField
from .models import CellManufacturer, CellModel, Cell, CellTest
from rest_framework import serializers


class CellManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellManufacturer
        fields = ["id", "name"]


class CellModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellModel
        fields = [
            "id",
            "name",
            "model_number",
            "form_factor",
            "manufacturer",
            "chemistry",
            "nominal_capacity",
            "minimum_capacity",
            "nominal_voltage",
            "charging_voltage",
            "cutoff_voltage",
            "max_charge_current",
            "max_discharge_current",
            "peak_discharge_current",
            "minimum_temperature",
            "maximum_temperature",
            "weight",
            "height",
            "width",
            "depth",
            "diameter",
            "description",
        ]
        validators = []


class CellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cell
        fields = ["id", "serial_number", "model", "color"]


class CellTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellTest
        fields = [
            "id",
            "elapsed_time",
            "high_voltage",
            "low_voltage",
            "discharge_rate",
            "calculated_capacity",
        ]

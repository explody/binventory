from .models import CellModel, Cell, CellTest, CellManufacturer
from rest_framework import serializers


class NestedCellModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellModel
        fields = ["id", "name", "form_factor", "manufacturer", "nominal_capacity"]


class NestedCellManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellManufacturer
        fields = ["id", "name"]


class NestedCellSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cell
        fields = ["id", "serial_number"]


class NestedCellTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = CellTest
        fields = [
            "id",
            "elapsed_time",
            "high_voltage",
            "low_voltage",
            "discharge_rate",
            "calculated_capacity",
        ]

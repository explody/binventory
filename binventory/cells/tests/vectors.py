import random
from core.constants import *
from faker import Factory

faker = Factory.create()


def valid_cell_specs():
    ff = random.randint(1, 3)
    if ff == FF_CYLINDER:
        return {
            "form_factor": ff,
            "diameter": random.randint(30, 80),
            "height": random.randint(30, 200),
            "width": None,
            "depth": None,
        }
    else:
        return {
            "form_factor": ff,
            "height": random.randint(30, 200),
            "width": random.randint(30, 200),
            "depth": random.randint(30, 200),
            "diameter": None,
        }


MANUFACTURERS = {
    "manufacturer1": {
        "name": "Battery Company, Inc.",
    },
    "manufacturer2": {
        "name": "Electronics Company, Inc.",
    },
}

MODELS = {
    "model1": {
        "name": "Sanyo 18650",
        "model_number": "UR18650A",
        "form_factor": FF_CYLINDER,
        "chemistry": BATTERY_CHEM_LI_ION,
        "nominal_capacity": 2250,
        "minimum_capacity": None,
        "nominal_voltage": 3.6,
        "charging_voltage": 4.2,
        "cutoff_voltage": 2.5,
        "max_charge_current": 1510,
        "max_discharge_current": 2150,
        "peak_discharge_current": None,
        "minimum_temperature": -20,
        "maximum_temperature": 40,
        "weight": 43,
        "height": 64.8,
        "diameter": 18.1,
        "description": "A standard, good, 18650 cell",
    },
    "model2": {
        "name": "Battery Co Cube",
        "model_number": "1x2x3x4x5x",
        "form_factor": FF_PRISMATIC,
        "chemistry": BATTERY_CHEM_LIFEPO,
        "nominal_capacity": 8000,
        "minimum_capacity": None,
        "nominal_voltage": 3.7,
        "charging_voltage": 4.2,
        "cutoff_voltage": 2.5,
        "max_charge_current": 1510,
        "max_discharge_current": 2150,
        "peak_discharge_current": None,
        "minimum_temperature": -20,
        "maximum_temperature": 40,
        "weight": 430,
        "height": 120,
        "width": 120,
        "depth": 120,
        "description": "A made up cube",
    },
    "update1": {
        "name": "UPD Sanyo 18650",
        "model_number": "UPDUR18650A",
        "form_factor": FF_CYLINDER,
        "chemistry": BATTERY_CHEM_LIFEPO,
        "nominal_capacity": 2251,
        "minimum_capacity": None,
        "nominal_voltage": 3.7,
        "charging_voltage": 4.2,
        "cutoff_voltage": 2.8,
        "max_charge_current": 1515,
        "max_discharge_current": 2155,
        "peak_discharge_current": 2600,
        "minimum_temperature": -25,
        "maximum_temperature": 45,
        "weight": 44.0,
        "height": 64.8,
        "diameter": 18.1,
        "description": "UPD A standard, good, 18650 cell",
    },
}

CELLS = {
    "cell1": {"serial_number": "a1b2c3d4e5", "color": COLOR_BLUE},
    "cell2": {"serial_number": "xxxx88888", "color": COLOR_RED},
    "update1": {"serial_number": "yyyyy1111", "color": COLOR_ORANGE},
}

import django_filters
from django.db.models import Q
from library.filters import BaseFilterSet, CreatedUpdatedFilterSet

from .models import CellManufacturer, CellModel, Cell, CellTest


class CellManufacturerFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = CellManufacturer
        fields = [
            "name",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(Q(name__icontains=value))


class CellModelFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = CellModel
        fields = [
            "slug",
            "name",
            "manufacturer",
            "description",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(slug__icontains=value)
            | Q(name__icontains=value)
            | Q(manufacturer__icontains=value)
            | Q(description__icontains=value)
        )


class CellFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = Cell
        fields = [
            "serial_number",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(Q(serial_number__icontains=value))


class CellTestFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = CellTest
        fields = [
            "calculated_capacity",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(Q(calculated_capacity__icontains=value))

from cells import filters
from cells.models import Cell, CellManufacturer, CellModel, CellTest
from library.api import ModelViewSet
from rest_framework.permissions import IsAuthenticated

from . import serializers


class CellManufacturerViewSet(ModelViewSet):
    queryset = CellManufacturer.objects.all()
    serializer_class = serializers.CellManufacturerSerializer
    filter_class = filters.CellManufacturerFilterSet


class CellModelViewSet(ModelViewSet):
    queryset = CellModel.objects.all()
    serializer_class = serializers.CellModelSerializer
    filter_class = filters.CellModelFilterSet


class CellViewSet(ModelViewSet):
    permission_classes = [IsAuthenticated]
    queryset = Cell.objects.select_related("model")
    serializer_class = serializers.CellSerializer
    filter_class = filters.CellFilterSet
    lookup_value_regex = "\d+"


class CellTestSet(ModelViewSet):
    queryset = CellTest.objects.select_related("cell")
    serializer_class = serializers.CellTestSerializer
    filter_class = filters.Cell

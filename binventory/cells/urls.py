from core.views import APIRootView
from library.routers import BinvAppRouter
from rest_framework import routers

from . import views


class CellsRootView(routers.APIRootView):
    """
    Cells API root view.
    """

    def get_view_name(self):
        return "Cells"


router = BinvAppRouter(trailing_slash=False)
# router.APIRootView = CellsRootView

router.register(r"", views.CellViewSet)
router.register(r"/models", views.CellModelViewSet)
router.register(r"/manufacturers", views.CellManufacturerViewSet)

app_name = "cells"
urlpatterns = router.urls

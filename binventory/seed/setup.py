from django.core import management

default_fixtures = []


def basic_setup(factory, cls, vector):
    # Randomized objects
    factory_objects = factory.create_batch(5)

    # Specific objects
    defined_objects = {}
    for k, v in vector.items():
        defined_objects[k] = cls.objects.create(**v)
        defined_objects[k].save()

    return factory_objects, defined_objects


def load_fixtures(fixtures=None):
    if not fixtures:
        fixtures = default_fixtures
    for f in fixtures:
        management.call_command("loaddata", f, verbosity=0)

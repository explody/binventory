import datetime
import io
import random
import tempfile

import factory
import pytz
from cells.models import Cell, CellManufacturer, CellModel, CellTest
from core.constants import *
from django.contrib.auth.hashers import make_password
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.text import slugify
from faker import Faker
from PIL import Image
from users.models import (
    AppUser,
    AnonymousUser,
    AppUserProfile,
    AnonymousProfile,
    APIToken,
    AppUser,
    AppUserPreferences,
)


# For future use
def create_image():
    with tempfile.NamedTemporaryFile(suffix=".png", delete=False) as f:
        image = Image.new("RGB", (200, 200), "blue")
        image.save(f, "PNG")

    return open(f.name, mode="rb")


def create_image_bytes(size=(100, 100), image_mode="RGB", image_format="PNG"):
    """
    Generate a test image, returning the filename that it was saved as.

    If ``storage`` is ``None``, the BytesIO containing the image data will be passed instead.
    """
    data = io.BytesIO()
    Image.new(image_mode, size).save(data, image_format)
    data.seek(0)
    return data.read()


def create_image_string(size=(100, 100), image_mode="RGB", image_format="PNG"):
    """
    Generate a test image, returning the filename that it was saved as.

    If ``storage`` is ``None``, the BytesIO containing the image data will be passed instead.
    """
    data = io.StringIO()
    Image.new(image_mode, size).save(data, image_format)
    data.seek(0)
    return data


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppUser

    password = factory.LazyAttribute(lambda p: make_password("Gah1Ohe7"))
    username = factory.Faker("user_name")
    email = factory.Faker("email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")
    is_staff = False
    is_superuser = False
    is_active = True


class UserProfileFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppUserProfile

    # user = factory.SubFactory(UserFactory)
    timezone = factory.Faker("timezone")
    language = factory.Faker("language_code")


class AppUserPreferencesFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AppUserPreferences

    user = factory.SubFactory(UserFactory)


class AnonymousAccountFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = AnonymousUser


class APITokenFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = APIToken

    user = factory.SubFactory(UserFactory)
    created = datetime.datetime.now(pytz.utc)
    expires = factory.Faker("future_datetime", end_date="+1h", tzinfo=pytz.utc)
    key = factory.Faker("password", length=40, special_chars=False)
    write_enabled = True


class CellManufacturerFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CellManufacturer

    name = factory.Faker("company")


class CellModelFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = CellModel

    class Params:
        width_rect = factory.Faker("pyint", min_value=30, max_value=200)
        depth_rect = factory.Faker("pyint", min_value=30, max_value=200)
        dia_cyl = factory.Faker("pyint", min_value=40, max_value=80)

    name = factory.Faker("vin")  # might as well use a VIN

    description = factory.Faker("text")

    nominal_capacity = factory.Faker("pyint", min_value=1800, max_value=3500)
    minimum_capacity = factory.Faker("pyint", min_value=1000, max_value=1500)

    nominal_voltage = factory.Faker("pyfloat", min_value=2.0, max_value=4.2)
    charging_voltage = factory.Faker("pyfloat", min_value=4.2, max_value=4.7)
    cutoff_voltage = factory.Faker("pyfloat", min_value=1.8, max_value=2.0)

    max_charge_current = factory.Faker("pyint", min_value=1, max_value=10)
    max_discharge_current = factory.Faker("pyint", min_value=1, max_value=10)
    peak_discharge_current = factory.Faker("pyint", min_value=10, max_value=15)

    weight = factory.Faker("pyint", min_value=30, max_value=3000)
    height = factory.Faker("pyint", min_value=30, max_value=200)
    # width = factory.Faker("pyint", min_value=30, max_value=200)
    width = factory.LazyAttribute(
        lambda a: a.width_rect if a.form_factor is not FF_CYLINDER else None
    )
    depth = factory.LazyAttribute(
        lambda a: a.depth_rect if a.form_factor is not FF_CYLINDER else None
    )
    # depth = factory.Faker("pyint", min_value=30, max_value=200)
    diameter = factory.LazyAttribute(
        lambda a: a.dia_cyl if a.form_factor is FF_CYLINDER else None
    )
    # diameter = factory.Faker("pyint", min_value=40, max_value=80)

    # constant maps

    chemistry = factory.LazyAttribute(lambda p: random.randint(1, len(BATTERY_CHEM)))
    form_factor = factory.LazyAttribute(lambda p: random.randint(1, len(FORM_FACTOR)))

    manufacturer = factory.SubFactory(random.choice([CellManufacturerFactory]))

    def __init__(self, *args, **kwargs):
        super().__init__(self, *args, **kwargs)

    # @factory.post_generation
    # def registrations(self, create, extracted, **kwargs):
    #     if not create:
    #         return
    #     if extracted:
    #         for registration in extracted:
    #             self.registrations.add(registration)


class CellFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Cell

    serial_number = factory.Faker("passport_number")
    model = factory.SubFactory(random.choice([CellModelFactory]))
    color = factory.LazyAttribute(lambda p: random.randint(1, len(COLORS)))

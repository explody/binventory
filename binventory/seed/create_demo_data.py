import pprint
import random
from django.conf import settings
from seed.factories import CellFactory, CellManufacturerFactory, CellModelFactory
from core.constants import *


def cell_specs():
    ff = random.randint(1, 3)
    if ff == FF_CYLINDER:
        return {
            "form_factor": ff,
            "diameter": random.randint(30, 80),
            "height": random.randint(30, 200),
            "width": None,
            "depth": None,
        }
    else:
        return {
            "form_factor": ff,
            "height": random.randint(30, 200),
            "width": random.randint(30, 200),
            "depth": random.randint(30, 200),
            "diameter": None,
        }


manus = []

print("Creating manufacturers")
for c in range(1, 20):
    manus.append(CellManufacturerFactory())

print("Creating cell models")
models = []
for c in range(1, 50):
    cell_data = cell_specs()

    cell_data["manufacturer"] = manus[random.randint(0, 18)]

    pprint.pp(cell_data)
    models.append(CellModelFactory(**cell_data))

print("Creating cells")
for c in range(1, 10000):
    CellFactory(model=models[random.randint(0, 48)])

from rest_framework import serializers

from .models import Pack, PackManufacturer


class NestedPackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pack
        fields = ["id", "name", "manufacturer", "nominal_capacity"]


class NestedPackManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PackManufacturer
        fields = ["id", "name"]

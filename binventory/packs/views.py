from library.api import ModelViewSet
from packs import filters
from packs.models import Pack, PackManufacturer
from rest_framework.permissions import IsAuthenticated

from . import serializers


class PackViewSet(ModelViewSet):
    queryset = Pack.objects.all()
    serializer_class = serializers.PackSerializer
    filter_class = filters.PackFilterSet


class PackManufacturerViewSet(ModelViewSet):
    queryset = PackManufacturer.objects.all()
    serializer_class = serializers.PackManufacturerSerializer
    filter_class = filters.PackManufacturerFilterSet

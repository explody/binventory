import django_filters
from django.db.models import Q
from library.filters import BaseFilterSet, CreatedUpdatedFilterSet

from .models import Pack, PackManufacturer


class PackFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = Pack
        fields = [
            "slug",
            "name",
            "manufacturer",
            "serial_number",
            "description",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(
            Q(slug__icontains=value)
            | Q(name__icontains=value)
            | Q(manufacturer__icontains=value)
            | Q(serial_number__icontains=value)
            | Q(description__icontains=value)
        )


class PackManufacturerFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = PackManufacturer
        fields = [
            "name",
        ]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(Q(name__icontains=value))

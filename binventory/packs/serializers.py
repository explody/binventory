from rest_framework import serializers

from .models import Pack, PackManufacturer


class PackManufacturerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PackManufacturer
        fields = ["id", "name"]


class PackSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pack
        fields = [
            "id",
            "name",
            "model_number",
            "manufacturer",
            "nominal_capacity",
            "minimum_capacity",
            "nominal_voltage",
            "charging_voltage",
            "cutoff_voltage",
            "max_charge_current",
            "max_discharge_current",
            "peak_discharge_current",
            "minimum_temperature",
            "maximum_temperature",
            "weight",
            "height",
            "width",
            "depth",
            "diameter",
            "description",
        ]
        validators = []

from library.routers import BinvAppRouter
from rest_framework import routers

from . import views

router = BinvAppRouter(trailing_slash=False)
# router.APIRootView = CellsRootView

router.register(r"", views.PackViewSet)

app_name = "packs"
urlpatterns = router.urls

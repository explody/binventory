from cells.models import Cell, CellManufacturer, CellModel
from core.constants import *
from django.core.exceptions import FieldError, ValidationError
from django.test import TestCase
from library.testing import ModelTestCases
from seed.factories import CellFactory, CellManufacturerFactory, CellModelFactory


class CellManufacturerTest(ModelTestCases):
    def setUp(self):
        self.instance = CellManufacturerFactory(name="Battery Company, Inc.")
        self.expected_data = {
            "type": CellManufacturer,
            "slug": "battery-company-inc",
            "absolute_url": "/cells/manufacturers/{}/".format(self.instance.pk),
            "string_repr": "Battery Company, Inc.",
        }

    def test_manufacturer_basics(self):
        self.assertIsInstance(self.instance, self.expected_data["type"])

        self.assertEqual(self.instance.name, self.expected_data["string_repr"])
        self.assertEqual(str(self.instance), self.expected_data["string_repr"])
        self.assertEqual(self.instance.slug, self.expected_data["slug"])


class CellModelTest(ModelTestCases):
    def setUp(self):
        self.manufacturer = CellManufacturerFactory(name="Battery Company, Inc.")
        self.instance = CellModelFactory(manufacturer=self.manufacturer)
        self.expected_data = {
            "type": CellModel,
            "absolute_url": "/cells/models/{}/".format(self.instance.pk),
        }

    def test_model_basics(self):
        self.assertIsInstance(self.instance, self.expected_data["type"])
        self.assertIsInstance(self.instance.manufacturer, CellManufacturer)

        self.assertEqual(
            self.instance.get_absolute_url(), self.expected_data["absolute_url"]
        )

        self.assertIsInstance(self.instance.name, str)
        self.assertIsInstance(self.instance.description, str)

        self.assertIsInstance(self.instance.nominal_capacity, int)
        self.assertIsInstance(self.instance.minimum_capacity, int)
        self.assertIsInstance(self.instance.nominal_voltage, float)
        self.assertIsInstance(self.instance.charging_voltage, float)
        self.assertIsInstance(self.instance.cutoff_voltage, float)

        self.assertIsInstance(self.instance.max_charge_current, float)
        self.assertIsInstance(self.instance.max_discharge_current, float)
        self.assertIsInstance(self.instance.peak_discharge_current, float)

        self.assertIsInstance(self.instance.weight, float)
        self.assertIsInstance(self.instance.height, float)

        if self.instance.form_factor == FF_CYLINDER:
            self.assertIsNone(self.instance.width)
            self.assertIsNone(self.instance.depth)
            self.assertIsInstance(self.instance.diameter, float)
        else:
            self.assertIsInstance(self.instance.width, float)
            self.assertIsInstance(self.instance.depth, float)
            self.assertIsNone(self.instance.diameter)

    def test_model_validations(self):
        with self.assertRaises(ValidationError):
            CellModelFactory(nominal_capacity=1, minimum_capacity=2)

        with self.assertRaises(ValidationError):
            CellModelFactory(charging_voltage=1, nominal_voltage=2)

        with self.assertRaises(ValidationError):
            CellModelFactory(charging_voltage=1, cutoff_voltage=2)

        with self.assertRaises(ValidationError):
            CellModelFactory(peak_discharge_current=1, max_discharge_current=2)


class CellTest(ModelTestCases):
    def setUp(self):
        self.manufacturer = CellManufacturerFactory(name="Battery Company, Inc.")
        self.model = CellModelFactory(manufacturer=self.manufacturer)
        self.instance = CellFactory(manufacturer=self.manufacturer, model=self.model)

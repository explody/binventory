import random

from cells.models import Cell, CellManufacturer, CellModel
from cells.nested_serializers import NestedCellModelSerializer
from core.constants import *
from django.urls import reverse
from library.testing import APIViewTestCases
from rest_framework import status
from seed.factories import CellFactory, CellManufacturerFactory, CellModelFactory
from seed.setup import load_fixtures

from .vectors import CELLS, MANUFACTURERS, MODELS


def setUpModule():
    load_fixtures()


class CellAPITest(APIViewTestCases.APIViewTestCase):
    model = Cell

    @classmethod
    def setUpTestData(cls):
        c1 = dict(CELLS["cell1"])
        c2 = dict(CELLS["cell2"])

        c1["model"] = CellModelFactory()
        CellFactory(**c1)

        cls.model2 = CellModelFactory()
        cls.model3 = CellModelFactory()

        c2["model"] = cls.model2.id
        cls.create_data = [c2]

        cls.update_data = dict(CELLS["update1"])
        cls.update_data["model"]: cls.model3.id

        cls.brief_fields = ["id", "serial_number"]


class CellManufacturerAPITest(APIViewTestCases.APIViewTestCase):
    model = CellManufacturer

    @classmethod
    def setUpTestData(cls):
        m1 = MANUFACTURERS["manufacturer1"].copy()
        cls.manufacturer1 = CellManufacturerFactory(**m1)
        cls.create_data = [MANUFACTURERS["manufacturer2"].copy()]
        cls.brief_fields = ["id", "name"]


class CellModelAPITest(APIViewTestCases.APIViewTestCase):
    model = CellModel

    @classmethod
    def setUpTestData(cls):
        man1 = MANUFACTURERS["manufacturer1"].copy()
        mod1 = MODELS["model1"].copy()
        mod2 = MODELS["model2"].copy()
        mod1["manufacturer"] = CellManufacturerFactory(**man1)
        mod2["manufacturer"] = mod1["manufacturer"].id
        cls.model1 = CellModelFactory(**mod1)

        cls.create_data = [mod2]
        cls.update_data = MODELS["update1"].copy()

        cls.brief_fields = NestedCellModelSerializer.Meta.fields

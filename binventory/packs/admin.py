from django import forms
from django.contrib import admin
from library.admin import BatteryModelAdminForm

from .models import Pack, PackManufacturer


class PackAdminForm(BatteryModelAdminForm):
    class Meta(BatteryModelAdminForm.Meta):
        model = Pack
        fields = [
            "name",
            "serial_number",
            "manufacturer",
            "series",
            "parallel",
        ] + BatteryModelAdminForm.Meta.fields
        validators = []


@admin.register(Pack)
class PackAdmin(admin.ModelAdmin):
    form = PackAdminForm
    list_display = ["name", "manufacturer", "nominal_capacity"]
    readonly_fields = ["created", "last_updated"]
    search_fields = ["name", "manufacturer"]


class PackManufacturerAdminForm(forms.ModelForm):
    class Meta:
        model = PackManufacturer
        fields = ["name"]


@admin.register(PackManufacturer)
class PackManufacturerAdmin(admin.ModelAdmin):
    form = PackManufacturerAdminForm
    list_display = [
        "name",
    ]
    readonly_fields = ["created", "last_updated"]

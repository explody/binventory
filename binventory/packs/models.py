from core.constants import *
from django.contrib.contenttypes.fields import GenericRelation
from django.core.exceptions import ValidationError
from django.db.models import (
    PROTECT,
    CharField,
    FloatField,
    ForeignKey,
    IntegerField,
    PositiveIntegerField,
    TextField,
)
from django.utils.translation import gettext as _
from django_extensions.db.fields import AutoSlugField
from library.models import BaseBattery, BaseManufacturer, BaseModel
from library.utils import od_to_choices


class PackManufacturer(BaseManufacturer):
    class Meta:
        verbose_name_plural = _("Pack Manufacturers")


class Pack(BaseBattery):
    slug = AutoSlugField(populate_from=["name"], blank=True)
    name = CharField(
        _("Model Name"), max_length=255, blank=False, null=False, unique=True
    )
    serial_number = CharField(_("Serial Number"), max_length=255, blank=True, null=True)
    manufacturer = ForeignKey(to=PackManufacturer, on_delete=PROTECT)
    series = IntegerField(_("Cells in Series"))
    parallel = IntegerField(_("Cells in Parallel"))

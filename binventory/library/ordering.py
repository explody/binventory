import re


def naturalize(value, max_length, integer_places=8):
    """
    Take an alphanumeric string and prepend all integers to `integer_places` places to ensure
    the strings are ordered naturally. For example:

    site9router21
    site10router4
    site10router19

    becomes:
    site00000009router00000021
    site00000010router00000004
    site00000010router00000019

    :param value: The value to be naturalized
    :param max_length: The maximum length of the returned string. Characters beyond this length
           will be stripped.
    :param integer_places: The number of places to which each integer will be expanded.
           (Default: 8)
    """
    if not value:
        return value
    output = []
    for segment in re.split(r'(\d+)', value):
        if segment.isdigit():
            output.append(segment.rjust(integer_places, '0'))
        elif segment:
            output.append(segment)
    ret = ''.join(output)

    return ret[:max_length]

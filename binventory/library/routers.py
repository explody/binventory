from django.urls import re_path
from rest_framework.routers import DynamicRoute, Route, SimpleRouter


class BinvAppRouter(SimpleRouter):
    """
    A router that handles base routes like "" better than default
    """

    def __init__(self, trailing_slash=False):
        super().__init__(trailing_slash=trailing_slash)

    def get_urls(self):
        """
        Use the registered viewsets to generate a list of URL patterns.
        """
        ret = []

        for prefix, viewset, basename in self.registry:
            lookup = self.get_lookup_regex(viewset)
            routes = self.get_routes(viewset)

            for route in routes:
                # Only actions which actually exist on the viewset will be bound
                mapping = self.get_method_map(viewset, route.mapping)
                if not mapping:
                    continue

                regex = route.url.format(
                    prefix=prefix, lookup=lookup, trailing_slash=self.trailing_slash
                )

                # Overrode this method just for this next bit.
                # Change this next bit to keep the slashes between a "no prefix"
                # view and the pk. The default removes it
                if prefix is None and regex[:2] == "^/":
                    regex = "^" + regex[2:]

                initkwargs = route.initkwargs.copy()
                initkwargs.update(
                    {
                        "basename": basename,
                        "detail": route.detail,
                    }
                )

                view = viewset.as_view(mapping, **initkwargs)
                name = route.name.format(basename=basename)
                ret.append(re_path(regex, view, name=name))

        return ret

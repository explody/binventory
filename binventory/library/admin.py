from django import forms
from django.contrib import admin

from .models import BaseBattery


class BatteryModelAdminForm(forms.ModelForm):
    class Meta:
        model = BaseBattery
        fields = [
            "nominal_capacity",
            "minimum_capacity",
            "nominal_voltage",
            "charging_voltage",
            "cutoff_voltage",
            "max_charge_current",
            "max_discharge_current",
            "peak_discharge_current",
            "minimum_temperature",
            "maximum_temperature",
            "weight",
            "height",
            "width",
            "depth",
            "diameter",
            "description",
        ]
        validators = []
        abstract = True

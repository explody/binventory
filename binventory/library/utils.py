import datetime
import hashlib
import json
import os
import re
import unicodedata

from collections import OrderedDict
from django.core.serializers import serialize
from django.db.models import Count, OuterRef, Subquery
from django.utils.deconstruct import deconstructible
from jinja2 import Environment


def csv_format(data):
    """Encapsulate any data which contains a comma within double quotes."""
    csv = []
    for value in data:
        # Represent None or False with empty string
        if value is None or value is False:
            csv.append("")
            continue

        # Convert dates to ISO format
        if isinstance(value, (datetime.date, datetime.datetime)):
            value = value.isoformat()

        # Force conversion to string first so we can check for any commas
        if not isinstance(value, str):
            value = "{}".format(value)

        # Double-quote the value if it contains a comma or line break
        if "," in value or "\n" in value:
            value = value.replace('"', '""')  # Escape double-quotes
            csv.append('"{}"'.format(value))
        else:
            csv.append("{}".format(value))

    return ",".join(csv)


def foreground_color(bg_color):
    """
    Return the ideal foreground color (black or white) for a given background color in
    hexadecimal RGB format.
    """
    bg_color = bg_color.strip("#")
    r, g, b = [int(bg_color[c : c + 2], 16) for c in (0, 2, 4)]
    if r * 0.299 + g * 0.587 + b * 0.114 > 186:
        return "000000"
    else:
        return "ffffff"


def dynamic_import(name):
    """Dynamically import a class from an absolute path string."""
    components = name.split(".")
    mod = __import__(components[0])
    for comp in components[1:]:
        mod = getattr(mod, comp)
    return mod


def get_subquery(model, field):
    """Return a Subquery suitable for annotating a child object count."""
    subquery = Subquery(
        model.objects.filter(**{field: OuterRef("pk")})
        .order_by()
        .values(field)
        .annotate(c=Count("*"))
        .values("c")
    )

    return subquery


def serialize_object(obj, extra=None, exclude=None):
    """
    Return a generic JSON representation of an object using Django's built-in serializer.

    (This is used for things like change logging, not the REST API.) Optionally include a
    dictionary to supplement the object data. A list of keys can be provided to exclude them
    from the returned dictionary. Private fields (prefaced with an underscore) are implicitly
    excluded.
    """
    json_str = serialize("json", [obj])
    data = json.loads(json_str)[0]["fields"]

    # Include any custom fields
    if hasattr(obj, "get_custom_fields"):
        data["custom_fields"] = {field: str(value) for field, value in obj.cf.items()}

    # Append any extra data
    if extra is not None:
        data.update(extra)

    # Copy keys to list to avoid 'dictionary changed size during iteration' exception
    for key in list(data):
        # Private fields shouldn't be logged in the object change
        if isinstance(key, str) and key.startswith("_"):
            data.pop(key)

        # Explicitly excluded keys
        if isinstance(exclude, (list, tuple)) and key in exclude:
            data.pop(key)

    return data


def dict_to_filter_params(d, prefix=""):
    """
    Translate a dictionary of attributes to a nested set of parameters suitable for
    QuerySet filtering. For example:

        {
            "name": "Foo",
            "rack": {
                "facility_id": "R101"
            }
        }

    Becomes:

        {
            "name": "Foo",
            "rack__facility_id": "R101"
        }

    And can be employed as filter parameters:

        Device.objects.filter(**dict_to_filter(attrs_dict))
    """
    params = {}
    for key, val in d.items():
        k = prefix + key
        if isinstance(val, dict):
            params.update(dict_to_filter_params(val, k + "__"))
        else:
            params[k] = val
    return params


def deepmerge(original, new):
    """Deep merge two dictionaries (new into original) and return a new dict."""
    merged = OrderedDict(original)
    for key, val in new.items():
        if (
            key in original
            and isinstance(original[key], dict)
            and isinstance(val, dict)
        ):
            merged[key] = deepmerge(original[key], val)
        else:
            merged[key] = val
    return merged


def render_jinja2(template_code, context):
    """
    Render a Jinja2 template with the provided context.

    Return the rendered content.
    """
    return Environment().from_string(source=template_code).render(**context)


def prepare_cloned_fields(instance):
    """
    Compile an object's `clone_fields` list into a string of URL query parameters.

    Tags are automatically cloned where applicable.
    """
    params = {}
    for field_name in getattr(instance, "clone_fields", []):
        field = instance._meta.get_field(field_name)
        field_value = field.value_from_object(instance)

        # Swap out False with URL-friendly value
        if field_value is False:
            field_value = ""

        # Omit empty values
        if field_value not in (None, ""):
            params[field_name] = field_value

    # Concatenate parameters into a URL query string
    param_string = "&".join(["{}={}".format(k, v) for k, v in params.items()])

    return param_string


def shallow_compare_dict(source_dict, destination_dict, exclude=None):
    """
    Return a new dictionary of the different keys.

    The values of `destination_dict` are returned. Only the equality of the first layer of
    keys/values is checked. `exclude` is a list or tuple of keys to be ignored.
    """
    difference = {}

    for key in destination_dict:
        if source_dict.get(key) != destination_dict[key]:
            if isinstance(exclude, (list, tuple)) and key in exclude:
                continue
            difference[key] = destination_dict[key]

    return difference


def flatten_dict(d, prefix="", separator="."):
    """
    Flatten netsted dictionaries into a single level by joining key names with a separator.

    :param d: The dictionary to be flattened :param prefix: Initial prefix (if any) :param
    separator: The character to use when concatenating key names
    """
    ret = {}
    for k, v in d.items():
        key = separator.join([prefix, k]) if prefix else k
        if type(v) is dict:
            ret.update(flatten_dict(v, prefix=key))
        else:
            ret[key] = v
    return ret


def json_changelog_to_html(jlog):
    log_message = ""

    for entry in jlog:
        for k in ["Changed", "Added", "Deleted"]:
            key = k.lower()
            if key in entry:
                sub_message = ""
                if "name" in entry[key]:
                    sub_message = (
                        ' on related <span class="subobj-name">{}</span> '
                        'instance "<span class="subobj-inst">{}</span>"'.format(
                            entry[key]["name"], entry[key]["object"]
                        )
                    )

                if "fields" in entry[key]:
                    log_message += (
                        '{} <span class="obj-change">{}</span>{}.<br />'.format(
                            k, ", ".join(entry[key]["fields"]), sub_message
                        )
                    )

    return log_message


def get_unique_field_set(model):
    unique_field_set = []

    if model._meta.unique_together:
        for ut in model._meta.unique_together:
            unique_field_set += list(ut)

    for field in model._meta.get_fields():
        if getattr(field, "unique", False):
            unique_field_set.append(field.name)

    return list(set(unique_field_set))


def camelcase_to_underscore(word):
    s1 = re.compile("(.)([A-Z][a-z]+)").sub(r"\1_\2", word)
    return re.compile("([a-z0-9])([A-Z])").sub(r"\1_\2", s1).lower()


def od_to_choices(od):
    return [
        (k, v.get("name", "unknown") if isinstance(v, dict) else v)
        for k, v in od.items()
    ]


def strip_accents(s):
    return "".join(
        c for c in unicodedata.normalize("NFD", s) if unicodedata.category(c) != "Mn"
    )


# This is redundant to filer.models.File.generate_sha1, but that method only operates on
# File instances.  We need to be able to calculate sha1 on unsaved files.
def calculate_sha1(file):
    sha = hashlib.sha1()
    file.seek(0)
    while True:
        buf = file.read(104857600)
        if not buf:
            break
        sha.update(buf)
    # to make sure later operations can read the whole file
    file.seek(0)
    return sha.hexdigest()


def hashed_directory_path(instance):
    return [instance.sha1[i : i + 2] for i in range(0, len(instance.sha1[:4]), 2)]


def hashed_file_name(instance, filename=None):
    if filename:
        _, ext = os.path.splitext(filename)
    elif getattr(instance, "original_filename", False):
        _, ext = os.path.splitext(instance.original_filename)
    else:
        ext = ""
    return (
        "{}{}".format(instance.short_hash, ext.lower()) if ext else instance.short_hash
    )


def hashed_file_path(instance, filename):
    file_path = hashed_directory_path(instance)
    file_path.append(hashed_file_name(instance, filename))
    file_path.insert(0, getattr(instance, "DIRECTORY_PREFIX", ""))
    return os.path.join(*file_path)


def is_taggable(obj):
    """Return True if the instance can have Tags assigned to it; False otherwise."""
    if hasattr(obj, "tags"):
        if issubclass(obj.tags.__class__, _TaggableManager):
            return True
        # TaggableManager has been replaced with a DummyQuerySet prior to object deletion
        if isinstance(obj.tags, DummyQuerySet):
            return True
    return False


def image_upload(instance, filename):
    """Return a path for uploading image attchments."""
    path = "image-attachments/"

    # Rename the file to the provided name, if any. Attempt to preserve the file extension.
    extension = filename.rsplit(".")[-1].lower()
    if instance.name and extension in ["bmp", "gif", "jpeg", "jpg", "png"]:
        filename = ".".join([instance.name, extension])
    elif instance.name:
        filename = instance.name

    return "{}{}_{}_{}".format(
        path, instance.content_type.name, instance.object_id, filename
    )


@deconstructible
class FeatureQuery:
    """
    Helper class that delays evaluation of the registry contents for the functionality store
    until it has been populated.
    """

    def __init__(self, feature):
        self.feature = feature

    def __call__(self):
        return self.get_query()

    def get_query(self):
        """Given an extras feature, return a Q object for content type lookup."""
        query = Q()
        for app_label, models in registry["model_features"][self.feature].items():
            query |= Q(app_label=app_label, model__in=models)

        return query


def extras_features(*features):
    """Decorator used to register extras provided features to a model."""

    def wrapper(model_class):
        # Initialize the model_features store if not already defined
        if "model_features" not in registry:
            registry["model_features"] = {
                f: collections.defaultdict(list) for f in EXTRAS_FEATURES
            }
        for feature in features:
            if feature in EXTRAS_FEATURES:
                app_label, model_name = model_class._meta.label_lower.split(".")
                registry["model_features"][feature][app_label].append(model_name)
            else:
                raise ValueError("{} is not a valid extras feature!".format(feature))
        return model_class

    return wrapper

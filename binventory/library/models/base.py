from django.db import models
from django.urls import reverse
from django.utils.translation import gettext as _
from library.utils import serialize_object

from .models import ObjectChange

__all__ = ("BaseInternalModel", "BaseModel", "ChangeLoggedModel")


class BaseInternalModel(models.Model):
    """Add basic common methods for DRY."""

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def __unicode__(self):
        return "%s" % self.__str__()

    def __str__(self):
        return "%s" % self.name if hasattr(self, "name") else super().__str__()

    @classmethod
    def get_field_label(cls, field):
        return _(cls._meta.get_field(field).verbose_name).title()

    class Meta:
        abstract = True


class BaseModel(BaseInternalModel):
    """Add basic common methods for DRY. w/ Expectation of related views"""

    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_updated = models.DateTimeField(auto_now=True, blank=True, null=True)

    def get_absolute_url(self):
        return reverse(
            "{}:{}-detail".format(
                self._meta.app_label, self.__class__.__name__.lower()
            ),
            args=(
                getattr(self, self.url_key) if hasattr(self, "url_key") else self.pk,
            ),
        )

    def get_update_url(self):
        return reverse(
            "{}:{}_edit".format(self._meta.app_label, self.__class__.__name__.lower()),
            args=(
                getattr(self, self.url_key) if hasattr(self, "url_key") else self.pk,
            ),
        )

    class Meta:
        abstract = True


class ChangeLoggedModel(models.Model):
    """
    An abstract model which adds fields to store the creation and last-updated times for an
    object.

    Both fields can be null to facilitate adding these fields to existing instances via a
    database migration.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    created = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    last_updated = models.DateTimeField(auto_now=True, blank=True, null=True)

    class Meta:
        abstract = True

    def to_objectchange(self, action):
        """
        Return a new ObjectChange representing a change made to this object.

        This will typically be called automatically by
        extras.middleware.ChangeLoggingMiddleware.
        """
        return ObjectChange(
            changed_object=self,
            object_repr=str(self),
            action=action,
            object_data=serialize_object(self),
        )

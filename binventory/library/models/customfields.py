from collections import OrderedDict
from datetime import date

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.validators import ValidationError
from django.db import models
from library.choices import *
from library.utils import FeatureQuery


#
# Custom fields
#


class CustomFieldModel(models.Model):
    _cf = None

    class Meta:
        abstract = True

    def cache_custom_fields(self):
        """Cache all custom field values for this instance."""
        self._cf = {
            field.name: value for field, value in self.get_custom_fields().items()
        }

    @property
    def cf(self):
        """Name-based CustomFieldValue accessor for use in templates."""
        if self._cf is None:
            self.cache_custom_fields()
        return self._cf

    def get_custom_fields(self):
        """
        Return a dictionary of custom fields for a single object in the form {<field>: value}.
        """
        fields = CustomField.objects.get_for_model(self)

        # If the object exists, populate its custom fields with values
        if hasattr(self, "pk"):
            values = self.custom_field_values.all()
            values_dict = {cfv.field_id: cfv.value for cfv in values}
            return OrderedDict([(field, values_dict.get(field.pk)) for field in fields])
        else:
            return OrderedDict([(field, None) for field in fields])


class CustomFieldManager(models.Manager):
    use_in_migrations = True

    def get_for_model(self, model):
        """Return all CustomFields assigned to the given model."""
        content_type = ContentType.objects.get_for_model(model._meta.concrete_model)
        return self.get_queryset().filter(obj_type=content_type)


class CustomField(models.Model):
    obj_type = models.ManyToManyField(
        to=ContentType,
        related_name="custom_fields",
        verbose_name="Object(s)",
        limit_choices_to=FeatureQuery("custom_fields"),
        help_text="The object(s) to which this field applies.",
    )
    type = models.CharField(
        max_length=50,
        choices=CustomFieldTypeChoices,
        default=CustomFieldTypeChoices.TYPE_TEXT,
    )
    name = models.CharField(max_length=50, unique=True)
    label = models.CharField(
        max_length=50,
        blank=True,
        help_text="Name of the field as displayed to users (if not provided, "
        "the field's name will be used)",
    )
    description = models.CharField(max_length=200, blank=True)
    required = models.BooleanField(
        default=False,
        help_text="If true, this field is required when creating new objects "
        "or editing an existing object.",
    )
    filter_logic = models.CharField(
        max_length=50,
        choices=CustomFieldFilterLogicChoices,
        default=CustomFieldFilterLogicChoices.FILTER_LOOSE,
        help_text="Loose matches any instance of a given string; exact "
        "matches the entire field.",
    )
    default = models.CharField(
        max_length=100,
        blank=True,
        help_text='Default value for the field. Use "true" or "false" for booleans.',
    )
    weight = models.PositiveSmallIntegerField(
        default=100, help_text="Fields with higher weights appear lower in a form."
    )

    objects = CustomFieldManager()

    class Meta:
        ordering = ["weight", "name"]

    def __str__(self):
        return self.label or self.name.replace("_", " ").capitalize()

    def serialize_value(self, value):
        """Serialize the given value to a string suitable for storage as a CustomFieldValue."""
        if value is None:
            return ""
        if self.type == CustomFieldTypeChoices.TYPE_BOOLEAN:
            return str(int(bool(value)))
        if self.type == CustomFieldTypeChoices.TYPE_DATE:
            # Could be date/datetime object or string
            try:
                return value.strftime("%Y-%m-%d")
            except AttributeError:
                return value
        if self.type == CustomFieldTypeChoices.TYPE_SELECT:
            # Could be ModelChoiceField or TypedChoiceField
            return str(value.id) if hasattr(value, "id") else str(value)
        return value

    def deserialize_value(self, serialized_value):
        """Convert a string into the object it represents depending on the type of field."""
        if serialized_value == "":
            return None
        if self.type == CustomFieldTypeChoices.TYPE_INTEGER:
            return int(serialized_value)
        if self.type == CustomFieldTypeChoices.TYPE_BOOLEAN:
            return bool(int(serialized_value))
        if self.type == CustomFieldTypeChoices.TYPE_DATE:
            # Read date as YYYY-MM-DD
            return date(*[int(n) for n in serialized_value.split("-")])
        if self.type == CustomFieldTypeChoices.TYPE_SELECT:
            return self.choices.get(pk=int(serialized_value))
        return serialized_value


class CustomFieldValue(models.Model):
    field = models.ForeignKey(
        to="library.CustomField", on_delete=models.CASCADE, related_name="values"
    )
    obj_type = models.ForeignKey(
        to=ContentType, on_delete=models.PROTECT, related_name="+"
    )
    obj_id = models.PositiveIntegerField()
    obj = GenericForeignKey(ct_field="obj_type", fk_field="obj_id")
    serialized_value = models.CharField(max_length=255)

    class Meta:
        ordering = ("obj_type", "obj_id", "pk")  # (obj_type, obj_id) may be non-unique
        unique_together = ("field", "obj_type", "obj_id")

    def __str__(self):
        return "{} {}".format(self.obj, self.field)

    @property
    def value(self):
        return self.field.deserialize_value(self.serialized_value)

    @value.setter
    def value(self, value):
        self.serialized_value = self.field.serialize_value(value)

    def save(self, *args, **kwargs):
        # Delete this object if it no longer has a value to store
        if self.pk and self.value is None:
            self.delete()
        else:
            super().save(*args, **kwargs)


class CustomFieldChoice(models.Model):
    field = models.ForeignKey(
        to="library.CustomField",
        on_delete=models.CASCADE,
        related_name="choices",
        limit_choices_to={"type": CustomFieldTypeChoices.TYPE_SELECT},
    )
    value = models.CharField(max_length=100)
    weight = models.PositiveSmallIntegerField(
        default=100, help_text="Higher weights appear lower in the list"
    )

    class Meta:
        ordering = ["field", "weight", "value"]
        unique_together = ["field", "value"]

    def __str__(self):
        return self.value

    def clean(self):
        if self.field.type != CustomFieldTypeChoices.TYPE_SELECT:
            raise ValidationError(
                "Custom field choices can only be assigned to selection fields."
            )

    def delete(self, using=None, keep_parents=False):
        # When deleting a CustomFieldChoice, delete all CustomFieldValues which point to it
        pk = self.pk
        super().delete(using, keep_parents)
        CustomFieldValue.objects.filter(
            field__type=CustomFieldTypeChoices.TYPE_SELECT, serialized_value=str(pk)
        ).delete()

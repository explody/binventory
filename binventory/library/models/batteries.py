from core.constants import *
from django.contrib.contenttypes.fields import GenericRelation
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import CharField, FloatField, IntegerField, TextField
from django.utils.translation import gettext as _
from django_extensions.db.fields import AutoSlugField
from library.models import BaseInternalModel, ChangeLoggedModel
from library.utils import od_to_choices


class BaseManufacturer(BaseInternalModel, ChangeLoggedModel):
    slug = AutoSlugField(populate_from=["name"], blank=True)
    name = CharField(_("Name"), max_length=255)

    class Meta:
        verbose_name_plural = _("Battery Manufacturers")
        abstract = True

    def __str__(self):
        return "{}".format(self.name)


class BaseBattery(BaseInternalModel, ChangeLoggedModel):
    class Meta:
        abstract = True

    manufacturer = None
    nominal_capacity = IntegerField(_("Nominal Capacity"))
    minimum_capacity = IntegerField(_("Minimum Capacity"), blank=True, null=True)
    nominal_voltage = FloatField(_("Nominal Voltage"))
    charging_voltage = FloatField(_("Charging Voltage"), blank=True, null=True)
    cutoff_voltage = FloatField(_("Cutoff Voltage"), blank=True, null=True)
    max_charge_current = IntegerField(_("Max Charge Current"), blank=True, null=True)
    max_discharge_current = IntegerField(
        _("Max Disharge Current"), blank=True, null=True
    )
    peak_discharge_current = IntegerField(
        _("Peak Disharge Current"), blank=True, null=True
    )

    minimum_temperature = IntegerField(_("Minimum Temperature"), blank=True, null=True)
    maximum_temperature = IntegerField(_("Maximum Temperature"), blank=True, null=True)

    weight = FloatField(_("Weight"), blank=True, null=True)
    height = FloatField(_("Height"), blank=True, null=True)
    width = FloatField(_("Width"), blank=True, null=True)
    depth = FloatField(_("Depth"), blank=True, null=True)
    diameter = FloatField(_("Diameter"), blank=True, null=True)

    description = TextField(
        max_length=8192, blank=True, verbose_name=_("Full Description")
    )

    def clean(self):
        if (
            self.nominal_capacity and self.minimum_capacity
        ) and self.nominal_capacity < self.minimum_capacity:
            raise ValidationError(
                _("Minimum capacity cannot be lower than nominal capacity")
            )

        if (self.charging_voltage and self.nominal_voltage) and (
            self.charging_voltage < self.nominal_voltage
            or self.charging_voltage < self.cutoff_voltage
        ):
            raise ValidationError(
                _("Charging voltage cannot be lower than nominal or cutoff voltage")
            )

        if (
            self.nominal_voltage and self.cutoff_voltage
        ) and self.nominal_voltage < self.cutoff_voltage:
            raise ValidationError(
                _("Nominal voltage cannot be lower than cutoff voltage")
            )

        if (
            self.peak_discharge_current and self.max_discharge_current
        ) and self.peak_discharge_current < self.max_discharge_current:
            raise ValidationError(
                _("Peak discharge current cannot be lower than max discharge current")
            )

        if self.form_factor in [FF_PRISMATIC, FF_POUCH] and self.diameter:
            raise ValidationError(_("Only cylindrical cells can have a diameter value"))

        if self.form_factor == FF_CYLINDER and (self.width or self.depth):
            raise ValidationError(
                _("Cylindrical cells do not have width or depth values")
            )

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

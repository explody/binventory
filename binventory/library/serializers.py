import base64
import imghdr
import os
import uuid

from django.apps import apps
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.base import ContentFile
from library.utils import camelcase_to_underscore, get_unique_field_set

from rest_framework.serializers import (
    PrimaryKeyRelatedField,
    ModelSerializer,
    Field,
    ChoiceField,
    SerializerMethodField,
    ImageField,
)


class ConstantReferenceField(ChoiceField):
    def __init__(self, *args, **kwargs):
        self.const = args[0]
        super().__init__(*args, **kwargs)

    def to_internal_value(self, data):
        if isinstance(data, dict):
            if "id" in data:
                const_id = data["id"]
            else:
                raise KeyError("No ID given")
        elif isinstance(data, str) or isinstance(data, int):
            const_id = data
        else:
            const_id = None

        try:
            const_id = int(const_id)
        except (TypeError, ValueError):
            self.fail("incorrect_type", data_type=type(const_id).__name__)

        return super().to_internal_value(const_id)

    def to_representation(self, value):
        if isinstance(self.const[value], dict):
            return {**self.const[value], **{"id": value}}
        else:
            return value


class WritableIDRelatedField(PrimaryKeyRelatedField):
    def __init__(self, **kwargs):
        self.serializer = kwargs.pop("serializer", None)
        if "pk_field" not in kwargs:
            kwargs["pk_field"] = "id"
        super().__init__(**kwargs)

    def use_pk_only_optimization(self):
        return False

    def to_internal_value(self, data):
        if isinstance(data, dict):
            try:
                pk_field_val = data[self.pk_field]
            except KeyError:
                raise KeyError("No such key")
        else:
            pk_field_val = data

        try:
            return self.get_queryset().get(pk=pk_field_val)
        except ObjectDoesNotExist:
            self.fail("does_not_exist", pk_value=pk_field_val)
        except (TypeError, ValueError):
            self.fail("incorrect_type", data_type=type(pk_field_val).__name__)

    def to_representation(self, value):
        if self.serializer is not None:
            return self.serializer(instance=value).data
        return value.pk


class WritableThroughField(Field):
    def __init__(self, **kwargs):
        self.to_model = kwargs.pop("to_model", None)
        self.through_model = kwargs.pop("through_model", None)
        self.serializer = kwargs.pop("serializer", None)
        self.many = kwargs.pop("many", False)
        self.writable_to_model = kwargs.pop("writable_to_model", False)

        super().__init__(**kwargs)

    def use_pk_only_optimization(self):
        return False

    def to_internal_value(self, data):
        return data

    def to_representation(self, value):
        if self.serializer is not None:
            if self.many:
                # The name of the related class must match the name of the through model field
                relation = camelcase_to_underscore(value.instance.__class__.__name__)
                return [
                    self.serializer(
                        instance=i,
                        through_model=self.through_model,
                        to_model=self.to_model,
                    ).data
                    for i in self.get_queryset().filter(**{relation: value.instance})
                ]
            else:
                return self.serializer(
                    instance=value,
                    through_model=self.through_model,
                    to_model=self.to_model,
                ).data

        return value

    def get_queryset(self):
        model = apps.get_model(self.through_model)
        return model.objects.all()


class MetaRelatedModelSerializer(ModelSerializer):
    def to_internal_value(self, data):
        return super().to_internal_value(data)

    def to_representation(self, instance):
        serialized = super().to_representation(instance)
        serialized["meta"] = self.Meta.meta_serializer(instance.meta).data
        return serialized

    def create(self, validated_data):
        meta = validated_data.pop("meta", None)
        instance = super().create(validated_data)

        # meta instance is created as a 1-to-1 upon save of the parent object, so it's always
        # an update
        if meta is not None:
            metas = self.Meta.meta_serializer(instance.meta)
            instance.meta = metas.update(instance.meta, meta)
        return instance

    def update(self, instance, validated_data):
        meta = validated_data.pop("meta", None)
        instance = super().update(instance, validated_data)
        if meta is not None:
            metas = self.Meta.meta_serializer(instance.meta)
            instance.meta = metas.update(instance.meta, meta)
        return instance


class UpdatableSlugSerializer(ModelSerializer):
    def update(self, instance, validated_data):
        # Setting slug to None causes AutoSlugField to recalculate it on save()
        if "slug" not in validated_data or getattr(validated_data, "slug", None) in (
            "",
            None,
        ):
            instance.slug = None

        return super().update(instance, validated_data)


class NestedThroughModelSerializer(ModelSerializer):
    class Meta:
        fields = []

    def __init__(self, *args, **kwargs):
        # We don't need 'to_model' here so remove it so it doesn't cause a kwarg error in the
        # super() call.
        _ = kwargs.pop("to_model", None)

        # Set the model for this serializer from the given model name
        setattr(self.Meta, "model", apps.get_model(kwargs.pop("through_model")))

        # Set fields for the serializer
        self.Meta.fields = kwargs.pop("through_model_fields", [])

        super().__init__(*args, **kwargs)


class HasThroughModelSerializer(ModelSerializer):
    # value comes from 'get_meta()'
    meta = SerializerMethodField()

    custom_kwargs = ["through_model", "to_model", "through_model_fields"]

    def __init__(self, *args, **kwargs):
        # Through model data will be under 'meta'
        if "meta" not in self.Meta.fields:
            self.Meta.fields.append("meta")

        # Preserve the original args/kwargs for the through model serializer
        self.meta_args = args
        self.meta_kwargs = kwargs.copy()
        self.meta_kwargs["through_model_fields"] = self.through_model_fields

        # The original instance passed to the serializer will be of the "through model",
        # not the "to model". But, since there's a foreign key in the through model pointing
        # to the to model, we'll swap out the given instance for its related "to" instance
        # The field name on the "through model" MUST match the lowercase name of the "to model"
        to_inst_attr = camelcase_to_underscore(kwargs["to_model"].split(".")[1])
        kwargs["instance"] = getattr(kwargs["instance"], to_inst_attr)

        # Continue to the next parent serializer, excluding custom kwargs
        super().__init__(
            *args, **{k: kwargs[k] for k in kwargs if k not in self.custom_kwargs}
        )

    def through_serializer(self, *args, **kwargs):
        # Enable using the nested through serializer but with the meta_args/meta_kwargs
        # always included
        args += self.meta_args
        kwargs = {**kwargs, **self.meta_kwargs}
        return NestedThroughModelSerializer(*args, **kwargs)

    def get_meta(self, obj):
        # The obj passed here is the original object passed to the child serializer,
        # which will be the "through model" instance.  The 'instance' kwarg will be set to
        # the "to model" instance in HasThroughModelSerializer, so we ignore 'obj' and pass
        # the adjusted args/kwargs
        return self.through_serializer().data


class ThroughFieldModelSerializer(ModelSerializer):
    def _pop_through_fields(self, validated_data):
        through_data = {}

        for key, field in self.fields.items():
            if isinstance(field, WritableThroughField):
                if key in validated_data:
                    through_data[key] = validated_data.pop(key)

        return through_data, validated_data

    def through_crud(self, instance, through_data):
        # The field name on the through model should match this
        from_model_field_name = camelcase_to_underscore(instance.__class__.__name__)

        for field_name, field_items in through_data.items():
            # Whether we try to create/update "to" models
            writable_to = getattr(self.fields[field_name], "writable_to_model", False)

            # If 'to' models are writable, there must be a serializer specified
            serializer = (
                self.fields[field_name].serializer.to_serializer
                if writable_to
                else None
            )

            # the through model
            through_model = apps.get_model(self.fields[field_name].through_model)

            # The "to" model itself
            to_model = apps.get_model(self.fields[field_name].to_model)
            to_model_unique_fieldset = get_unique_field_set(to_model)

            # Name of the field on the through model that holds the relation to the
            # 'unique_together[0]' on a through model should *always* contain
            # to_model_field_name and from_model_field_name
            # So, whichever item is not the from_model_field_name should be
            # the to_model_field_name
            tofrom_fields = list(through_model._meta.unique_together[0])
            tofrom_fields.remove(from_model_field_name)
            to_model_field_name = tofrom_fields[0]

            # grab PK field names
            to_model_pk_field = to_model._meta.pk.name
            through_model_pk_field = through_model._meta.pk.name

            # will hold the new list of added/updated through model instances
            new_through_instances = []

            # Fetch IDs if through models currently referencing this instance
            current_through_instances = through_model.objects.filter(
                **{from_model_field_name: instance}
            )

            # flag to determine whether we should update a given 'to' model or not
            do_update = True

            if not isinstance(field_items, list):
                field_items = [field_items]

            for field_data in field_items:
                # through model data
                if isinstance(field_data, dict):
                    through_model_data = field_data.pop("meta", None)

                    # ID of the "to" model, if it is given
                    to_id = field_data.get(to_model_pk_field, None)
                else:
                    through_model_data = None
                    try:
                        to_id = int(field_data)
                        do_update = False  # Only an ID was given, nothing to update
                    except ValueError:
                        raise ValueError("Field data must be a PK (int) or dict")

                # If we have no to_model ID and creating new to_models is disabled, then fail
                assert not (to_id is None and writable_to is False), (
                    "Nested writes of {} through field {} are not possible.  "
                    "Please add/modify instances directly through that model's "
                    "api view".format(to_model, field_name)
                )

                if to_id is None and writable_to:
                    # If we have no ID in the field, presume that we're adding a new 'to'
                    # instance but try to get it first to ensure there isn't an existing 'to'
                    # model
                    try:
                        # Try to get an existing to_model by searching on a unique set of fields
                        to_model_inst = to_model.objects.get(
                            **{
                                k: field_data[k]
                                for k in field_data
                                if k in to_model_unique_fieldset
                            }
                        )
                    except to_model.DoesNotExist:
                        do_update = False

                        # If the get failed, create the to_model with the serializer
                        create_serializer = serializer(
                            data=field_data, context=self.context
                        )
                        create_serializer.is_valid(raise_exception=True)
                        to_model_inst = create_serializer.save()
                else:
                    # We were given an ID, fetch the to_model
                    to_model_inst = to_model.objects.get(**{to_model_pk_field: to_id})

                if do_update and writable_to:
                    update_serializer = serializer(
                        instance=to_model_inst, data=field_data, context=self.context
                    )
                    update_serializer.is_valid(raise_exception=True)
                    to_model_inst = update_serializer.save()

                if through_model_data is not None:
                    through_id = through_model_data.get(
                        through_model._meta.pk.name, None
                    )

                    # We'll create through instances manually here as the only required fields
                    # are the to/from instances, which don't need validation. If the 'to' model
                    # was new, ignore any given through_id
                    if through_id is None or is_new:
                        # use get_or_create to ensure we've not been given an existing 'through'
                        # relationship missing its ID
                        through_model_inst, _ = through_model.objects.get_or_create(
                            **{
                                from_model_field_name: instance,
                                to_model_field_name: to_model_inst,
                            }
                        )
                    else:
                        through_model_inst = through_model.objects.get(
                            **{
                                through_model_pk_field: through_model_data[
                                    through_model_pk_field
                                ]
                            }
                        )

                    # Now update via the serializer to allow for validation on any other given
                    # fields
                    through_update_serializer = NestedThroughModelSerializer(
                        through_model=self.fields[field_name].through_model,
                        through_model_fields=self.fields[
                            field_name
                        ].serializer.through_model_fields,
                        instance=through_model_inst,
                        data=through_model_data,
                        context=self.context,
                    )
                    through_update_serializer.is_valid(raise_exception=True)
                    through_model_inst = through_update_serializer.save()

                else:
                    # No 'through' data was given, auto get/create a barebones through model
                    through_model_inst, _ = through_model.objects.get_or_create(
                        **{
                            from_model_field_name: instance,
                            to_model_field_name: to_model_inst,
                        }
                    )

                new_through_instances.append(through_model_inst)

            current_through_instances.exclude(
                id__in=[r.id for r in new_through_instances]
            ).delete()

        instance.refresh_from_db()

        return instance

    def update(self, instance, validated_data):
        through_data, validated_data = self._pop_through_fields(validated_data)
        instance = super().update(instance, validated_data)
        return self.through_crud(instance, through_data)

    def create(self, validated_data):
        through_data, validated_data = self._pop_through_fields(validated_data)
        instance = super().create(validated_data)
        return self.through_crud(instance, through_data)


def ids_from_dict(data):
    ids = []
    for item in data:
        if isinstance(item, dict):
            ids.append(item["id"])
        else:
            try:
                # Converting to int and back ensures that it's actually a number
                ids.append(str(int(item)))
            except ValueError:
                continue
    # Alternate one-liner with no data checking
    # [v['id'] for _k,v in data.items()]
    return ids


class Base64ImageField(ImageField):
    """
    A Django REST framework field for handling image-uploads through raw post data. It uses
    base64 for encoding and decoding the contents of the file.

    Heavily based on https://github.com/tomchristie/django-rest-framework/pull/1268  Updated for
    Django REST framework 3.
    """

    def to_internal_value(self, data):
        original_filename = self.parent.initial_filename()

        # Check if this is a base64 string
        if isinstance(data, str):
            # Check if the base64 string is in the "data:" format
            if "data:" in data and ";base64," in data:
                # Break out the header from the base64 content
                header, data = data.split(";base64,")

            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(data)
            except TypeError:
                self.fail("invalid_image")

            # Generate file name if the parent serializer was not given a name
            if original_filename is None:
                original_filename = str(uuid.uuid4())[
                    :12
                ]  # 12 characters are more than enough

            original_filename, ext = os.path.splitext(original_filename)

            data = ContentFile(
                decoded_file,
                "%s.%s"
                % (
                    original_filename,
                    ext.lstrip(".")
                    or self.get_file_extension(original_filename, decoded_file),
                ),
            )

        return super().to_internal_value(data)

    @staticmethod
    def get_file_extension(file_name, decoded_file):
        extension = imghdr.what(file_name, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        return extension

import hashlib
import random

from django.core.mail import send_mail
from django.template.loader import render_to_string

from django.conf import settings


class AccountDefaultHookSet(object):
    @staticmethod
    def send_email(
        subject_template, message_template, context, to_addr, from_addr=None
    ):
        if from_addr is None:
            from_addr = settings.NOREPLY_EMAIL
        subject = render_to_string(subject_template, context)
        subject = "".join(subject.splitlines())  # remove superfluous line breaks
        message = render_to_string(message_template, context)
        send_mail(subject, message, from_addr, to_addr)

    def send_invitation_email(self, to_addr, context):
        self.send_email(
            "account/email/invite_user_subject.txt",
            "account/email/invite_user.txt",
            context,
            to_addr,
        )

    def send_confirmation_email(self, to_addr, context):
        self.send_email(
            "account/email/email_confirmation_subject.txt",
            "account/email/email_confirmation_message.txt",
            context,
            to_addr,
        )

    def send_password_change_email(self, to_addr, context):
        self.send_email(
            "account/email/password_change_subject.txt",
            "account/email/password_change.txt",
            context,
            to_addr,
        )

    # password reset mails are sent by main django code

    @staticmethod
    def generate_random_token(extra=None, hash_func=hashlib.sha256):
        if extra is None:
            extra = []
        bits = extra + [str(random.SystemRandom().getrandbits(512))]
        return hash_func("".join(bits).encode("utf-8")).hexdigest()

    def generate_signup_code_token(self, email=None):
        extra = []
        if email:
            extra.append(email)
        return self.generate_random_token(extra)

    def generate_email_confirmation_token(self, email):
        return self.generate_random_token([email])

    @staticmethod
    def get_user_credentials(form, identifier_field):
        return {
            "username": form.cleaned_data[identifier_field],
            "password": form.cleaned_data["password"],
        }

    @staticmethod
    def account_delete_mark(deletion):
        deletion.user.is_active = False
        deletion.user.save()

    @staticmethod
    def account_delete_expunge(deletion):
        # Deleting users here is bad, as they may own content. Leaving them disabled is
        # sufficient.
        # deletion.user.delete()
        pass


class HookProxy(object):
    def __getattr__(self, attr):
        return getattr(settings.ACCOUNT_HOOKSET, attr)


hookset = HookProxy()

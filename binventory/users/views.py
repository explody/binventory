from library.api import ModelViewSet
from rest_framework.permissions import IsAuthenticated
from users import filters

from . import serializers
from .models import AppUser, AppUserPreferences, AppUserProfile


class UserViewSet(ModelViewSet):
    queryset = AppUser.objects.all()
    serializer_class = serializers.AppUserSerializer
    filter_class = filters.AppUserFilterSet

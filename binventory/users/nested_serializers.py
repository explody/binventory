from rest_framework import serializers

from .models import AppUser, AppUserPreferences, AppUserProfile


class NestedAppUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = ["id", "username", "email"]


class NestedAppUserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUserProfile
        fields = ["timezone", "language"]


class NestedAppUserPreferencesSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUserPreferences
        fields = ["data"]

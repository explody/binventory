from django.conf import settings
from django.db import models


class TimeZoneField(models.CharField):
    def __init__(self, *args, **kwargs):
        defaults = {
            "max_length": 100,
            "default": "",
            "choices": settings.TIMEZONES,
            "blank": True,
        }
        defaults.update(kwargs)
        return super().__init__(*args, **defaults)

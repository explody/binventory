import django_filters
from django.db.models import Q
from library.filters import BaseFilterSet, CreatedUpdatedFilterSet

from .models import AppUser


class AppUserFilterSet(BaseFilterSet, CreatedUpdatedFilterSet):
    q = django_filters.CharFilter(
        method="search",
        label="Search",
    )

    class Meta:
        model = AppUser
        fields = ["username", "email"]

    def search(self, queryset, name, value):
        if not value.strip():
            return queryset
        return queryset.filter(Q(username__icontains=value) | Q(email__icontains=value))

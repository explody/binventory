from library.routers import BinvAppRouter
from rest_framework import routers

from . import views

router = BinvAppRouter(trailing_slash=False)
router.register(r"", views.UserViewSet)

app_name = "users"
urlpatterns = router.urls

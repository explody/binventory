#
import binascii
import datetime
import os

import pytz
from django.conf import settings  # noqa
from django.contrib.auth.models import AbstractUser, AnonymousUser, User
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models import JSONField
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone, translation
from django.utils.translation import gettext as _
from library.models.base import BaseInternalModel, BaseModel, ChangeLoggedModel
from library.utils import flatten_dict
from users.fields import TimeZoneField

__all__ = ("AppUser", "AppUserProfile", "AppUserPreferences")


class AppUser(AbstractUser, models.Model):
    class Meta:
        app_label = "users"
        verbose_name_plural = _("Users")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)


class AppUserProfile(BaseInternalModel, ChangeLoggedModel):
    user = models.OneToOneField(
        settings.AUTH_USER_MODEL,
        related_name="profile",
        verbose_name=_("user"),
        on_delete=models.CASCADE,
    )
    timezone = TimeZoneField(_("timezone"))
    language = models.CharField(
        _("language"),
        max_length=10,
        choices=settings.LANGUAGES,
        default=settings.DEFAULT_LANGUAGE,
    )

    class Meta:
        verbose_name_plural = _("User Profiles")

    @classmethod
    def for_request(cls, request):
        user = getattr(request, "user", None)
        if user and user.is_authenticated:
            try:
                return AppUserProfile._default_manager.get(user=user)
            except AppUserProfile.DoesNotExist:
                pass
        return AnonymousProfile(request)

    @classmethod
    def create(cls, request=None, **kwargs):
        profile = cls(**kwargs)
        if "language" not in kwargs:
            if request is None:
                profile.language = settings.DEFAULT_LANGUAGE
            else:
                profile.language = translation.get_language_from_request(
                    request, check_path=True
                )
        profile.save()
        return profile

    def __str__(self):
        return str(self.user)

    def now(self):
        """
        Returns a timezone aware datetime localized to the account's timezone.
        """
        now = datetime.datetime.utcnow().replace(tzinfo=pytz.timezone("UTC"))
        tz = settings.TIME_ZONE if not self.timezone else self.timezone
        return now.astimezone(pytz.timezone(tz))

    def localtime(self, value):
        """
        Given a datetime object as value convert it to the timezone of
        the profile.
        """
        tz = settings.TIME_ZONE if not self.timezone else self.timezone
        if value.tzinfo is None:
            value = pytz.timezone(settings.TIME_ZONE).localize(value)
        return value.astimezone(pytz.timezone(tz))


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def user_post_save(sender, **kwargs):
    """
    After User.save is called we check to see if it was a created user. If so,
    we check if the User object wants profile creation. If all passes we
    create an Account object.

    We only run on user creation to avoid having to check for existence on
    each call to User.save.
    """

    # Disable post_save during manage.py loaddata
    if kwargs.get("raw", False):
        return False

    user, created = kwargs["instance"], kwargs["created"]
    disabled = getattr(
        user, "_disable_profile_creation", not settings.ACCOUNT_CREATE_ON_SAVE
    )
    if created and not disabled:
        AppUserProfile.create(user=user)


class AnonymousProfile:
    def __init__(self, request=None):
        super().__init__()
        self.user = AnonymousUser()
        self.timezone = settings.TIME_ZONE
        if request is None:
            self.language = settings.DEFAULT_LANGUAGE
        else:
            self.language = translation.get_language_from_request(
                request, check_path=True
            )

    def __str__(self):
        return "AnonymousProfile"

    def __unicode__(self):
        return "{}".format(self.__str__())


class AppUserPreferences(BaseInternalModel, ChangeLoggedModel):
    """This model stores arbitrary user-specific preferences in a JSON data structure."""

    user = models.OneToOneField(
        "users.AppUser", on_delete=models.CASCADE, related_name="preferences"
    )

    data = JSONField(default=dict)

    class Meta:
        ordering = ["user"]
        verbose_name = verbose_name_plural = "App Preferences"

    def __str__(self):
        return "Preferences for {}".format(self.user.username)

    def get(self, path, default=None):
        """
        Retrieve a configuration parameter specified by its dotted path.

        Example: userconfig.get('foo.bar.baz')
        :param path: Dotted path to the configuration key. For example, 'foo.bar' returns
        self.data['foo']['bar'].
        :param default: Default value to return for a nonexistent key (default: None).
        """
        d = self.data
        keys = path.split(".")

        # Iterate down the hierarchy, returning the default value
        # if any invalid key is encountered
        for key in keys:
            if type(d) is dict and key in d:
                d = d.get(key)
            else:
                return default

        return d

    def all(self):
        """Return a dictionary of all defined keys and their values."""
        return flatten_dict(self.data)

    def set(self, path, value, commit=False):
        """
        Define or overwrite a configuration parameter. Example:

        userconfig.set('foo.bar.baz', 123)  Leaf nodes (those which are not dictionaries of
        other nodes) cannot be overwritten as dictionaries. Similarly, branch nodes
        (dictionaries) cannot be overwritten as single values. (A TypeError exception will be
        raised.) In both cases, the existing key must first be cleared. This safeguard is in
        place to help avoid inadvertently overwriting the wrong key.  :param path: Dotted path
        to the configuration key. For example, 'foo.bar' sets self.data['foo']['bar']. :param
        value: The value to be written. This can be any type supported by JSON. :param commit:
        If true, the UserConfig instance will be saved once the new value has been applied.
        """
        d = self.data
        keys = path.split(".")

        # Iterate through the hierarchy to find the key we're setting. Raise TypeError
        # if we encounter any interim leaf nodes (keys which do not contain dictionaries).
        for i, key in enumerate(keys[:-1]):
            if key in d and type(d[key]) is dict:
                d = d[key]
            elif key in d:
                err_path = ".".join(path.split(".")[: i + 1])
                raise TypeError(
                    f"Key '{err_path}' is a leaf node; cannot assign new keys"
                )
            else:
                d = d.setdefault(key, {})

        # Set a key based on the last item in the path. Raise TypeError if attempting to
        # overwrite a non-leaf node.
        key = keys[-1]
        if key in d and type(d[key]) is dict:
            raise TypeError(f"Key '{path}' has child keys; cannot assign a value")
        else:
            d[key] = value

        if commit:
            self.save()

    def clear(self, path, commit=False):
        """
        Delete a configuration parameter specified by its dotted path. The key and any child
        keys will be deleted.
        Example: userconfig.clear('foo.bar.baz')  Invalid keys will be
        ignored silently.

        :param path: Dotted path to the configuration key. For example, 'foo.bar' deletes
        self.data['foo']['bar'].
        :param commit: If true, the UserConfig instance will be saved
        once the new value has been applied.
        """
        d = self.data
        keys = path.split(".")

        for key in keys[:-1]:
            if key not in d:
                break
            if type(d[key]) is dict:
                d = d[key]

        key = keys[-1]
        d.pop(key, None)  # Avoid a KeyError on invalid keys

        if commit:
            self.save()


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_app_preferences(instance, created, **kwargs):
    """Automatically create a new UserConfig when a new User is created."""
    if created:
        AppUserPreferences(user=instance).save()


class APIToken(BaseModel):
    """
    An API token used for user authentication.

    This extends the stock model to allow each user to have multiple tokens. It also supports
    setting an expiration time and toggling write ability.
    """

    user = models.ForeignKey(
        "users.AppUser", on_delete=models.CASCADE, related_name="tokens"
    )
    created = models.DateTimeField(auto_now_add=True)
    expires = models.DateTimeField(blank=True, null=True)
    key = models.CharField(
        max_length=40, unique=True, validators=[MinLengthValidator(40)]
    )
    write_enabled = models.BooleanField(
        default=True, help_text="Permit create/update/delete operations using this key"
    )
    description = models.CharField(max_length=200, blank=True)

    class Meta:
        pass

    def __str__(self):
        # Only display the last 24 bits of the token to avoid accidental exposure.
        return "{} ({})".format(self.key[-6:], self.user)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super().save(*args, **kwargs)

    @staticmethod
    def generate_key():
        # Generate a random 160-bit key expressed in hexadecimal.
        return binascii.hexlify(os.urandom(20)).decode()

    @property
    def is_expired(self):
        if self.expires is None or timezone.now() < self.expires:
            return False
        return True

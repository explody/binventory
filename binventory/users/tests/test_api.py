import random

from cells.nested_serializers import NestedCellModelSerializer
from core.constants import *
from django.urls import reverse
from library.testing import APIViewTestCases
from rest_framework import status
from seed.factories import UserFactory, UserProfileFactory
from seed.setup import load_fixtures
from users.models import AppUser

from .vectors import USERS


def setUpModule():
    load_fixtures()


class UserAPITest(APIViewTestCases.APIViewTestCase):
    model = AppUser

    @classmethod
    def setUpTestData(cls):
        u1 = dict(USERS["testuser1"])
        u2 = dict(USERS["testuser2"])

        UserFactory(**u1)

        cls.create_data = [u2]
        cls.update_data = dict(USERS["update1"])

        cls.brief_fields = ["id", "username", "email"]

        pass

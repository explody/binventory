# -*- coding: utf-8 -*-

import datetime

import pytz
from django.contrib.auth import get_user_model
from django.test import TestCase
from factory.faker import faker
from library.testing import ModelTestCases, TestCase
from seed.factories import AppUserPreferencesFactory, UserFactory
from users.models import AnonymousProfile, AnonymousUser, AppUserProfile

User = get_user_model()
faker_generator = faker.Faker()


class UserProfileTestCase(ModelTestCases.ModelTestCase):
    @classmethod
    def setUpTestData(cls):
        # Accounts are auto-created via signal
        cls.user = UserFactory(username="profiletester")

        cls.instance = cls.user.profile
        cls.instance.timezone = faker_generator.timezone()
        cls.instance.language = faker_generator.language_code()
        cls.instance.save()

        cls.expected_data = {
            "type": AppUserProfile,
            "string_repr": "profiletester",
        }

    def test_profile_now(self):
        self.assertIsInstance(self.instance.now(), datetime.datetime)

    def test_profile_localtime(self):
        now = datetime.datetime.now()
        self.assertIsInstance(self.instance.localtime(now), datetime.datetime)
        self.assertEqual(
            self.instance.localtime(now),
            now.astimezone(pytz.timezone(self.instance.timezone)),
        )


class AnonymousAccountTestCase(ModelTestCases.ModelTestCase):
    @classmethod
    def setUpTestData(cls):
        cls.instance = AnonymousProfile()

        cls.expected_data = {
            "type": AnonymousProfile,
            "string_repr": "AnonymousProfile",
        }


class AppUserPreferencesTest(TestCase):
    def setUp(self):
        user = UserFactory(username="profiletester")
        user.preferences.data = {
            "a": True,
            "b": {
                "foo": 101,
                "bar": 102,
            },
            "c": {
                "foo": {
                    "x": 201,
                },
                "bar": {
                    "y": 202,
                },
                "baz": {
                    "z": 203,
                },
            },
        }
        user.preferences.save()

        self.app_preferences = user.preferences

    def test_get(self):
        app_preferences = self.app_preferences

        # Retrieve root and nested values
        self.assertEqual(app_preferences.get("a"), True)
        self.assertEqual(app_preferences.get("b.foo"), 101)
        self.assertEqual(app_preferences.get("c.baz.z"), 203)

        # Invalid values should return None
        self.assertIsNone(app_preferences.get("invalid"))
        self.assertIsNone(app_preferences.get("a.invalid"))
        self.assertIsNone(app_preferences.get("b.foo.invalid"))
        self.assertIsNone(app_preferences.get("b.foo.x.invalid"))

        # Invalid values with a provided default should return the default
        self.assertEqual(app_preferences.get("invalid", "DEFAULT"), "DEFAULT")
        self.assertEqual(app_preferences.get("a.invalid", "DEFAULT"), "DEFAULT")
        self.assertEqual(app_preferences.get("b.foo.invalid", "DEFAULT"), "DEFAULT")
        self.assertEqual(app_preferences.get("b.foo.x.invalid", "DEFAULT"), "DEFAULT")

    def test_all(self):
        app_preferences = self.app_preferences
        flattened_data = {
            "a": True,
            "b.foo": 101,
            "b.bar": 102,
            "c.foo.x": 201,
            "c.bar.y": 202,
            "c.baz.z": 203,
        }

        # Retrieve a flattened dictionary containing all config data
        self.assertEqual(app_preferences.all(), flattened_data)

    def test_set(self):
        app_preferences = self.app_preferences

        # Overwrite existing values
        app_preferences.set("a", "abc")
        app_preferences.set("c.foo.x", "abc")
        self.assertEqual(app_preferences.data["a"], "abc")
        self.assertEqual(app_preferences.data["c"]["foo"]["x"], "abc")

        # Create new values
        app_preferences.set("d", "abc")
        app_preferences.set("b.baz", "abc")
        self.assertEqual(app_preferences.data["d"], "abc")
        self.assertEqual(app_preferences.data["b"]["baz"], "abc")

        # Set a value and commit to the database
        app_preferences.set("a", "def", commit=True)

        app_preferences.refresh_from_db()
        self.assertEqual(app_preferences.data["a"], "def")

        # Attempt to change a branch node to a leaf node
        with self.assertRaises(TypeError):
            app_preferences.set("b", 1)

        # Attempt to change a leaf node to a branch node
        with self.assertRaises(TypeError):
            app_preferences.set("a.x", 1)

    def test_clear(self):
        app_preferences = self.app_preferences

        # Clear existing values
        app_preferences.clear("a")
        app_preferences.clear("b.foo")
        self.assertTrue("a" not in app_preferences.data)
        self.assertTrue("foo" not in app_preferences.data["b"])
        self.assertEqual(app_preferences.data["b"]["bar"], 102)

        # Clear a non-existing value; should fail silently
        app_preferences.clear("invalid")

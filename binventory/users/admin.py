from django import forms
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as UserAdmin_
from django.contrib.auth.models import Group

from .models import APIToken, AppUser, AppUserPreferences


@admin.register(AppUser)
class AppUserAdmin(UserAdmin_):
    list_display = [
        "username",
        "first_name",
        "last_name",
        "is_superuser",
        "is_staff",
        "is_active",
    ]


class AppUserPreferencesInline(admin.TabularInline):
    model = AppUserPreferences
    readonly_fields = ("data",)
    can_delete = False
    verbose_name = "Preferences"


class APITokenAdminForm(forms.ModelForm):
    key = forms.CharField(
        required=False,
        help_text="If no key is provided, one will be generated automatically.",
    )

    class Meta:
        fields = ["user", "key", "write_enabled", "expires", "description"]
        model = APIToken


@admin.register(APIToken)
class APITokenAdmin(admin.ModelAdmin):
    form = APITokenAdminForm
    list_display = ["key", "user", "created", "expires", "write_enabled", "description"]


class AppUserPreferencesAdmin(admin.ModelAdmin):
    model = AppUserPreferences
    readonly_fields = ("data",)
    can_delete = False
    verbose_name = "Preferences"


class AccountAdmin(admin.ModelAdmin):
    raw_id_fields = ["user"]
    # inlines = (AppUserPreferencesInline,)


# admin.site.register(AppUser, AppUserAdmin)
admin.site.register(AppUserPreferences, AppUserPreferencesAdmin)
admin.site.unregister(Group)

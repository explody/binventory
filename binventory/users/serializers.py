from library.serializers import UpdatableSlugSerializer, WritableIDRelatedField
from rest_framework import serializers

from .models import AppUser
from .nested_serializers import (
    NestedAppUserPreferencesSerializer,
    NestedAppUserProfileSerializer,
)


class AppUserSerializer(serializers.ModelSerializer):
    profile = NestedAppUserProfileSerializer(required=False)
    preferences = NestedAppUserPreferencesSerializer(required=False)

    class Meta:
        model = AppUser
        fields = ["id", "username", "email", "profile", "preferences"]

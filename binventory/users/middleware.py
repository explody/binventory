from urllib.parse import urlparse, urlunparse

from django.conf import settings
from django.contrib import messages
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect, QueryDict
from django.urls import resolve, reverse
from django.utils import timezone, translation
from django.utils.cache import patch_vary_headers
from django.utils.deprecation import MiddlewareMixin as BaseMiddleware
from django.utils.translation import gettext_lazy as _

from users import signals
from users.models import AppUserProfile


class LocaleMiddleware(BaseMiddleware):
    """
    This is a very simple middleware that parses a request
    and decides what translation object to install in the current
    thread context depending on the user's account. This allows pages
    to be dynamically translated to the language the user desires
    (if the language is available, of course).
    """

    def get_language_for_user(self, request):
        if request.user.is_authenticated:
            try:
                account = AppUserProfile.objects.get(user=request.user)
                return account.language
            except AppUserProfile.DoesNotExist:
                pass
        return translation.get_language_from_request(request)

    def process_request(self, request):
        translation.activate(self.get_language_for_user(request))
        request.LANGUAGE_CODE = translation.get_language()

    def process_response(self, request, response):
        patch_vary_headers(response, ("Accept-Language",))
        response["Content-Language"] = translation.get_language()
        translation.deactivate()
        return response


class TimezoneMiddleware(BaseMiddleware):
    """
    This middleware sets the timezone used to display dates in
    templates to the user's timezone.
    """

    def process_request(self, request):
        try:
            profile = getattr(request.user, "profile", None)
        except AppUserProfile.DoesNotExist:
            pass
        else:
            if profile:
                tz = settings.TIME_ZONE if not profile.timezone else profile.timezone
                timezone.activate(tz)

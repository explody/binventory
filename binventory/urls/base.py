from cells.views import CellViewSet
from core.health import ping
from decorator_include import decorator_include
from django.conf import settings
from django.contrib import admin
from django.db import connection
from django.urls import include, path

admin.autodiscover()

urlpatterns = [
    path(r"admin/", admin.site.urls),
    path(r"api/v1/", include("core.urls")),
    path(r"api/v1/cells", include("cells.urls")),
    path(r"api/v1/packs", include("packs.urls")),
    path(r"api/v1/users", include("users.urls")),
    path(r"ping", ping),
]

if getattr(settings, "DEBUG", False):
    import debug_toolbar

    urlpatterns += [
        path(r"__debug__/", include(debug_toolbar.urls)),
    ]

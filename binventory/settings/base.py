"""
Django settings for My project.
"""
import os
import sys
from pathlib import Path

import environ
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import gettext_lazy as _

from .languages import DEFAULT_LANGUAGE, LANGUAGES
from .timezones import TIMEZONES

env = environ.Env()

try:
    from binventory import configuration
except ImportError:
    raise ImproperlyConfigured(
        "Unable to import configuration. Please make sure configuration.py  "
        "exists per the documentation."
    )

# LANGUAGES = languages.LANGUAGES
# TIMEZONES = timezones.TIMEZONES

# Get base dir using environ
BASE_DIR = Path(__file__).resolve().parent.parent.parent

# Cast project dirs as string for future manipulation
PROJECT_ROOT = BASE_DIR / "binventory"

APP_PREFIX = env.str(
    "APP_PREFIX",
    default=getattr(configuration, "APP_PREFIX", "/"),
)
APP_ENV = env.str(
    "APP_ENV",
    default=getattr(configuration, "APP_ENV", "local"),
)

ROOT_URLCONF = f"urls.{APP_ENV}"

sys.path.insert(1, str(PROJECT_ROOT))

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = env.str(
    "SECRET_KEY",
    default=getattr(configuration, "SECRET_KEY", None),
)

# SECURITY WARNING: don"t run with debug turned on in production!
DEBUG = env.bool(
    "DEBUG",
    default=getattr(configuration, "DEBUG", False),
)

AUTH_PASSWORD_VALIDATORS = [
    {
        "NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.MinimumLengthValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.CommonPasswordValidator",
    },
    {
        "NAME": "django.contrib.auth.password_validation.NumericPasswordValidator",
    },
]

# Our user model
AUTH_USER_MODEL = "users.AppUser"

# Set up authentication backends
AUTHENTICATION_BACKENDS = [
    "django.contrib.auth.backends.ModelBackend",
    "library.auth_backends.ViewExemptModelBackend",
]

ALLOWED_HOSTS = env.list(
    "ALLOWED_HOSTS",
    default=getattr(configuration, "ALLOWED_HOSTS", ["*"]),
)
INTERNAL_IPS = env.list(
    "INTERNAL_IPS",
    default=[
        "127.0.0.1",
    ],
)

CORS_ALLOW_CREDENTIALS = True

APPEND_SLASH = False

# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field
DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

LOGIN_URL = "admin:login"

BANNER_BOTTOM = getattr(configuration, "BANNER_BOTTOM", "")
BANNER_LOGIN = getattr(configuration, "BANNER_LOGIN", "")
BANNER_TOP = getattr(configuration, "BANNER_TOP", "")
BASE_PATH = getattr(configuration, "BASE_PATH", "")

CHANGELOG_RETENTION = getattr(configuration, "CHANGELOG_RETENTION", 365)

CORS_ALLOWED_ORIGINS = getattr(configuration, "CORS_ALLOWED_ORIGINS", [])
CORS_ALLOW_ALL_ORIGINS = getattr(configuration, "CORS_ALLOW_ALL_ORIGINS", False)
CORS_ALLOWED_ORIGIN_REGEXES = getattr(configuration, "CORS_ALLOWED_ORIGIN_REGEXES", [])
CORS_ORIGIN_WHITELIST = getattr(configuration, "CORS_ORIGIN_WHITELIST", [])

MAINTENANCE_MODE = getattr(configuration, "MAINTENANCE_MODE", False)
MAX_PAGE_SIZE = getattr(configuration, "MAX_PAGE_SIZE", 1000)
PAGINATE_COUNT = getattr(configuration, "PAGINATE_COUNT", 50)

# Application definition
INSTALLED_APPS = (
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "django.contrib.sites",
    "debug_toolbar",
    "django_extensions",
    "corsheaders",
    "rest_framework",
    "drf_spectacular",
    "drf_spectacular_sidecar",
    "taggit",
    "taggit_serializer",
    "core",
    "users",
    "library",
    "cells",
    "packs",
    "dj_rest_auth",
)

MIDDLEWARE = [
    "django.contrib.sessions.middleware.SessionMiddleware",
    "debug_toolbar.middleware.DebugToolbarMiddleware",
    "corsheaders.middleware.CorsMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
    "django.middleware.cache.UpdateCacheMiddleware",
    "core.middleware.AppIdMiddleware",
]

WSGI_APPLICATION = "wsgi.application"


TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "DIRS": [
            PROJECT_ROOT / "templates",
        ],
        # "APP_DIRS": True,  # must not be set when loaders is defined
        "OPTIONS": {
            "loaders": [
                (
                    "django.template.loaders.cached.Loader",
                    [
                        "django.template.loaders.filesystem.Loader",
                        "django.template.loaders.app_directories.Loader",
                    ],
                ),
            ],
            "context_processors": [
                # Insert your TEMPLATE_CONTEXT_PROCESSORS here or use this
                # list if you haven"t customized them:
                "django.contrib.auth.context_processors.auth",
                "django.template.context_processors.debug",
                "django.template.context_processors.i18n",
                "django.template.context_processors.media",
                "django.template.context_processors.static",
                "django.template.context_processors.request",
                "django.template.context_processors.tz",
                "django.contrib.messages.context_processors.messages",
                "core.context_processors.core_processor",
                "core.context_processors.global_variables",
            ],
            "debug": DEBUG,
        },
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/

LANGUAGE_CODE = "en"

LANGUAGES = (
    ("en", _("English")),
    ("nb", _("Norwegian")),
)

TIME_ZONE = "Europe/Oslo"
USE_I18N = True
USE_L10N = False
USE_TZ = True

DECIMAL_SEPARATOR = ","

# Static files (CSS, JavaScript, Images)
STATICFILES_DIRS = [
    PROJECT_ROOT / "global_staticfiles",
]
STATICFILES_FINDERS = (
    "django.contrib.staticfiles.finders.AppDirectoriesFinder",
    "django.contrib.staticfiles.finders.FileSystemFinder",
)
STATIC_URL = env.str("STATIC_URL", default="/static/")
STATIC_ROOT = env.str("STATIC_ROOT", default=PROJECT_ROOT / "site_media" / "static")
STATICFILES_STORAGE = env.str(
    "STATICFILES_STORAGE",
    default="django.contrib.staticfiles.storage.StaticFilesStorage",
)

MEDIA_URL = env.str("MEDIA_URL", default="/media/")
MEDIA_ROOT = env.str("MEDIA_ROOT", default=PROJECT_ROOT / "site_media" / "media")
DEFAULT_FILE_STORAGE = env.str(
    "DEFAULT_FILE_STORAGE", default="django.core.files.storage.FileSystemStorage"
)

SERVE_MEDIA_FILES = env.bool("SERVE_MEDIA", default=False)
SERVE_STATIC_FILES = env.bool("SERVE_STATIC", default=False)

# Where ViteJS assets are built.
# DJANGO_VITE_ASSETS_PATH = BASE_DIR / "frontend" / "dist"

# # If use HMR or not.
# DJANGO_VITE_DEV_MODE = DEBUG
# DJANGO_VITE_STATIC_URL_PREFIX = "vite"

# Include DJANGO_VITE_ASSETS_PATH into STATICFILES_DIRS to be copied inside
# when run command python manage.py collectstatic
# STATICFILES_DIRS += [DJANGO_VITE_ASSETS_PATH]

LOGGING_FORMATTER = env.str("LOGGING_FORMATTER", default="verbose")
# Logging setup
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "filters": {
        "require_debug": {
            "()": "django.utils.log.RequireDebugTrue",
        },
        "require_debug_false": {
            "()": "django.utils.log.RequireDebugFalse",
        },
        "skippped_logs": {"()": "core.logging_filters.DisabledLogFilter"},
    },
    "formatters": {
        "verbose": {
            "format": "%(levelname)s %(asctime)s %(module)s "
            "%(process)d %(thread)d %(message)s"
        },
    },
    "handlers": {
        "console": {
            "level": "INFO",
            "class": "logging.StreamHandler",
            "formatter": LOGGING_FORMATTER,
            "filters": ["skippped_logs"],
        }
    },
    "root": {"level": "INFO", "handlers": ["console"]},
}

DEFAULT_EXCEPTION_REPORTER_FILTER = "core.logging_filters.CustomExceptionFilter"

# Application settings #

DATE_FORMAT = "d.m.Y"
SHORT_DATE_FORMAT = "d.m.y"
DATE_INPUT_FORMATS = (
    "%d.%m.%Y",
    "%d.%m.%Y.",
    "%Y-%m-%d",
)

# Caching settings
CACHES = {
    "default": {
        "BACKEND": "django.core.cache.backends.memcached.PyMemcacheCache",
        "KEY_PREFIX": "binventory",
    },
}

# Add prefix for cookies so that there isn"t mixup of cookies
SESSION_COOKIE_PATH = APP_PREFIX
CSRF_COOKIE_PATH = APP_PREFIX

SESSION_SERIALIZER = "django.contrib.sessions.serializers.JSONSerializer"
# Expire session after 3 days to avoid long session storages
SESSION_COOKIE_AGE = 259200

SITE_ID = 1

EMAIL_PORT = 1025

# Disable patching of the settings files as it breaks when used with WSGI
DEBUG_TOOLBAR_PATCH_SETTINGS = env.bool("DT_PATCH_SETTINGS", default=False)

# Account settings
ACCOUNT_PASSWORD_EXPIRY = getattr(configuration, "ACCOUNT_PASSWORD_EXPIRY", 0)
ACCOUNT_PASSWORD_USE_HISTORY = getattr(
    configuration, "ACCOUNT_PASSWORD_USE_HISTORY", False
)
ACCOUNT_PASSWORD_STRIP = getattr(configuration, "ACCOUNT_PASSWORD_STRIP", True)
ACCOUNT_REMEMBER_ME_EXPIRY = getattr(
    configuration, "ACCOUNT_REMEMBER_ME_EXPIRY", 60 * 60 * 24 * 365 * 10
)
ACCOUNT_USER_DISPLAY = getattr(
    configuration, "ACCOUNT_USER_DISPLAY", lambda user: user.username
)
ACCOUNT_CREATE_ON_SAVE = getattr(configuration, "ACCOUNT_CREATE_ON_SAVE", True)
ACCOUNT_EMAIL_UNIQUE = getattr(configuration, "ACCOUNT_EMAIL_UNIQUE", True)
ACCOUNT_EMAIL_ALLOWED_DOMAINS = getattr(
    configuration, "ACCOUNT_EMAIL_ALLOWED_DOMAINS", []
)
ACCOUNT_EMAIL_CONFIRMATION_REQUIRED = getattr(
    configuration, "ACCOUNT_EMAIL_CONFIRMATION_REQUIRED", True
)
ACCOUNT_EMAIL_CONFIRMATION_EMAIL = getattr(
    configuration, "ACCOUNT_EMAIL_CONFIRMATION_EMAIL", True
)
ACCOUNT_EMAIL_CONFIRMATION_EXPIRATION = getattr(
    configuration, "ACCOUNT_EMAIL_CONFIRMATION_EXPIRATION", 1
)  # In hours
ACCOUNT_EMAIL_CONFIRMATION_AUTO_LOGIN = getattr(
    configuration, "ACCOUNT_EMAIL_CONFIRMATION_AUTO_LOGIN", False
)
ACCOUNT_NOTIFY_ON_PASSWORD_CHANGE = getattr(
    configuration, "ACCOUNT_NOTIFY_ON_PASSWORD_CHANGE", True
)
ACCOUNT_DELETION_EXPUNGE_HOURS = getattr(
    configuration, "ACCOUNT_DELETION_EXPUNGE_HOURS", 48
)
ACCOUNT_OPEN_SIGNUP = getattr(configuration, "ACCOUNT_OPEN_SIGNUP", False)
ACCOUNT_LOGIN_ON_SIGNUP = getattr(configuration, "ACCOUNT_LOGIN_ON_SIGNUP", False)
ACCOUNT_LOGOUT_CONFIRM = getattr(configuration, "ACCOUNT_LOGOUT_CONFIRM", False)

REST_FRAMEWORK = {
    "DEFAULT_AUTHENTICATION_CLASSES": (
        "core.api.APITokenAuthentication",
        "dj_rest_auth.jwt_auth.JWTCookieAuthentication",
    ),
    "DEFAULT_PERMISSION_CLASSES": [
        "rest_framework.permissions.DjangoModelPermissionsOrAnonReadOnly",
    ],
    "DEFAULT_RENDERER_CLASSES": [
        "rest_framework.renderers.JSONRenderer",
        "rest_framework.renderers.BrowsableAPIRenderer",
    ],
    "DEFAULT_PAGINATION_CLASS": "core.api.OptionalLimitOffsetPagination",
    "DEFAULT_SCHEMA_CLASS": "drf_spectacular.openapi.AutoSchema",
    "PAGE_SIZE": 100,
}

SPECTACULAR_SETTINGS = {
    "TITLE": "Binventory",
    "DESCRIPTION": "bat trees",
    # "VERSION": "0.0.1",
    "SERVE_INCLUDE_SCHEMA": False,
    # "SWAGGER_UI_DIST": "SIDECAR",  # shorthand to use the sidecar instead
    # "SWAGGER_UI_FAVICON_HREF": "SIDECAR",
    # "REDOC_DIST": "SIDECAR",
    # OTHER SETTINGS
}

REST_AUTH = {
    "LOGIN_SERIALIZER": "dj_rest_auth.serializers.LoginSerializer",
    "TOKEN_SERIALIZER": "dj_rest_auth.serializers.APITokenSerializer",
    "JWT_SERIALIZER": "dj_rest_auth.serializers.JWTSerializer",
    "JWT_SERIALIZER_WITH_EXPIRATION": "dj_rest_auth.serializers.JWTSerializerWithExpiration",
    "JWT_TOKEN_CLAIMS_SERIALIZER": "rest_framework_simplejwt.serializers.APITokenObtainPairSerializer",
    "USER_DETAILS_SERIALIZER": "dj_rest_auth.serializers.UserDetailsSerializer",
    "PASSWORD_RESET_SERIALIZER": "dj_rest_auth.serializers.PasswordResetSerializer",
    "PASSWORD_RESET_CONFIRM_SERIALIZER": "dj_rest_auth.serializers.PasswordResetConfirmSerializer",
    "PASSWORD_CHANGE_SERIALIZER": "dj_rest_auth.serializers.PasswordChangeSerializer",
    "REGISTER_SERIALIZER": "dj_rest_auth.registration.serializers.RegisterSerializer",
    "REGISTER_PERMISSION_CLASSES": ("rest_framework.permissions.AllowAny",),
    "TOKEN_MODEL": "users.models.APIToken",
    "TOKEN_CREATOR": "dj_rest_auth.utils.default_create_token",
    "PASSWORD_RESET_USE_SITES_DOMAIN": False,
    "OLD_PASSWORD_FIELD_ENABLED": False,
    "LOGOUT_ON_PASSWORD_CHANGE": False,
    "SESSION_LOGIN": True,
    "USE_JWT": True,
    "JWT_AUTH_COOKIE": "why",
    "JWT_AUTH_REFRESH_COOKIE": "thing",
    "JWT_AUTH_REFRESH_COOKIE_PATH": "/",
    "JWT_AUTH_SECURE": False,
    "JWT_AUTH_HTTPONLY": False,
    "JWT_AUTH_SAMESITE": "Lax",
    "JWT_AUTH_RETURN_EXPIRATION": False,
    "JWT_AUTH_COOKIE_USE_CSRF": False,
    "JWT_AUTH_COOKIE_ENFORCE_CSRF_ON_UNAUTHENTICATED": False,
}

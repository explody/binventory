import os

from .base import *

DEBUG = True
DJANGO_VITE_DEV_MODE = DEBUG

TEST_RUNNER = "rainbowtests.test.runner.RainbowDiscoverRunner"

ALLOWED_HOSTS = ["*"]

CORS_ALLOW_ALL_ORIGINS = True

ROOT_URLCONF = "urls.local"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.sqlite3",
        "NAME": BASE_DIR / "db.sqlite3",
    }
}

# We want full dbugging in development
LOGGING["root"]["level"] = "INFO"
# Normal formatting for local development
LOGGING["handlers"]["console"]["formatter"] = "verbose"

# Remove cached loader for local development
del TEMPLATES[0]["OPTIONS"]["loaders"]
TEMPLATES[0]["APP_DIRS"] = True
TEMPLATES[0]["OPTIONS"]["debug"] = True

# For local development we don't want to bother with interactive debuger PIN
os.environ["WERKZEUG_DEBUG_PIN"] = "off"

DEBUG_TOOLBAR_PATCH_SETTINGS = True

AUTH_PASSWORD_VALIDATORS = []

import os

from .base import *

DEBUG = True

ALLOWED_HOSTS = ["*"]

CORS_ALLOW_ALL_ORIGINS = True
CSRF_TRUSTED_ORIGINS = ["http://localhost:8000"]


ROOT_URLCONF = "urls.local"

DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "binventory",
        "USER": "binventory",
        "PASSWORD": "binventory",
        "HOST": "db",
        "PORT": "5432",
    }
}

# We want full dbugging in development
LOGGING["root"]["level"] = "INFO"
# Normal formatting for local development
LOGGING["handlers"]["console"]["formatter"] = "verbose"

# Remove cached loader for local development
TEMPLATES[0]["OPTIONS"].pop("loaders", None)
TEMPLATES[0]["APP_DIRS"] = True
TEMPLATES[0]["OPTIONS"]["debug"] = True

# For local development we don't want to bother with interactive debuger PIN
os.environ["WERKZEUG_DEBUG_PIN"] = "off"

DEBUG_TOOLBAR_PATCH_SETTINGS = True

AUTH_PASSWORD_VALIDATORS = []

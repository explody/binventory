from core.views import APIRootView
from dj_rest_auth.app_settings import api_settings
from dj_rest_auth.jwt_auth import get_refresh_view
from dj_rest_auth.views import (
    LoginView,
    LogoutView,
    PasswordChangeView,
    PasswordResetConfirmView,
    PasswordResetView,
    UserDetailsView,
)
from django.contrib import admin
from django.urls import include, path, re_path
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularJSONAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from rest_framework.schemas import get_schema_view
from rest_framework_simplejwt.views import TokenVerifyView

from .views import StatsView

app_name = "core"

urlpatterns = [
    path(r"", APIRootView.as_view(), name="api-root"),
    path("stats", StatsView.as_view()),
    path("schema", SpectacularJSONAPIView.as_view(), name="schema"),
    # Optional UI:
    path(
        "schema/swagger",
        SpectacularSwaggerView.as_view(url_name="core:schema"),
        name="swagger",
    ),
    path(
        "schema/redoc",
        SpectacularRedocView.as_view(url_name="core:schema"),
        name="redoc",
    ),
]

# This is pretty much only here so I can remove the trailing slashes
urlpatterns += [
    path("password/reset", PasswordResetView.as_view(), name="rest_password_reset"),
    path(
        "password/reset/confirm",
        PasswordResetConfirmView.as_view(),
        name="rest_password_reset_confirm",
    ),
    path("login", LoginView.as_view(), name="rest_login"),
    path("logout", LogoutView.as_view(), name="rest_logout"),
    path("whoami", UserDetailsView.as_view(), name="rest_user_details"),
    path("password/change", PasswordChangeView.as_view(), name="rest_password_change"),
]

if api_settings.USE_JWT:
    urlpatterns += [
        path("token/verify", TokenVerifyView.as_view(), name="token_verify"),
        path("token/refresh", get_refresh_view().as_view(), name="token_refresh"),
    ]

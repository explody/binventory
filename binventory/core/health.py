from django.db import connections
from django.http import HttpResponse


def ping(request):
    db_conn = connections["default"]
    try:
        db_conn.cursor()
        return HttpResponse(status=200)
    except Exception as e:
        return HttpResponse(status=503)

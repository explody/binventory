from collections import OrderedDict

from cells.models import Cell, CellManufacturer, CellModel
from django.http import HttpResponse
from rest_framework import serializers
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from .serializers import StatsSerializer


class APIRootView(APIView):
    _ignore_model_permissions = True
    exclude_from_schema = True

    def get_view_name(self):
        return "API Root"

    def get(self, request, format=None):
        return Response(
            OrderedDict(
                (
                    (
                        "core",
                        reverse("core:api-root", request=request, format=format),
                    ),
                    (
                        "cells",
                        reverse("core:cells:api-root", request=request, format=format),
                    ),
                    (
                        "schema",
                        reverse("core:schema", request=request, format=format),
                    ),
                    (
                        "redoc",
                        reverse("core:redoc", request=request, format=format),
                    ),
                    (
                        "swagger",
                        reverse("core:swagger", request=request, format=format),
                    ),
                )
            )
        )


def ping(request):
    """
    Health check url, it should return simplest and fastest response possible.

    It is used for checking availability
    """
    return HttpResponse("ok")


class StatsView(APIView):
    permission_classes = [IsAuthenticated]
    serializer_class = StatsSerializer

    def get(self, request):
        stats = {
            "cells": Cell.objects.all().count,
            "models": CellModel.objects.all().count,
            "manufacturers": CellManufacturer.objects.all().count,
        }
        return Response(StatsSerializer(stats).data)

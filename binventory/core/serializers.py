from rest_framework import serializers


class StatsSerializer(serializers.Serializer):
    cells = serializers.IntegerField()
    models = serializers.IntegerField()
    manufacturers = serializers.IntegerField()

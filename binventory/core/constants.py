from collections import OrderedDict
from django.utils.translation import gettext as _

FF_CYLINDER = 1
FF_PRISMATIC = 2
FF_POUCH = 3

FORM_FACTOR = OrderedDict(
    {
        FF_CYLINDER: _('Cylindrical'),
        FF_PRISMATIC: _('Prismatic'),
        FF_POUCH: _('Pouch')
    }
)

BATTERY_CHEM_LEAD_ACID = 1
BATTERY_CHEM_ALKALINE = 2
BATTERY_CHEM_NIZN = 3
BATTERY_CHEM_NIFE = 4
BATTERY_CHEM_NICAD = 5
BATTERY_CHEM_NIH2 = 6
BATTERY_CHEM_NIMH = 7
BATTERY_CHEM_LSD_NIMH = 8
BATTERY_CHEM_LI_ION = 9
BATTERY_CHEM_LTO = 10
BATTERY_CHEM_LIFEPO = 11
BATTERY_CHEM_LMO = 12

BATTERY_CHEM = OrderedDict(
    {
        BATTERY_CHEM_LEAD_ACID: {
            "name": _("Lead-acid"),
            "full_name": _("Lead-acid"),
        },
        BATTERY_CHEM_ALKALINE: {
            "name": _("RAM"),
            "full_name": _("Rechargeable Alkaline"),
        },
        BATTERY_CHEM_NIZN: {
            "name": _("NiZn"),
            "full_name": _("Nickel-zinc"),
        },
        BATTERY_CHEM_NIFE: {
            "name": _("NiFe"),
            "full_name": _("Nickel-iron"),
        },
        BATTERY_CHEM_NICAD: {
            "name": _("NiCad"),
            "full_name": _("Nickel-cadmium"),
        },
        BATTERY_CHEM_NIH2: {
            "name": _("NiH2"),
            "full_name": _("Nickel-hydrogen"),
        },
        BATTERY_CHEM_NIMH: {
            "name": _("NiMH"),
            "full_name": _("Nickel-metal hydride"),
        },
        BATTERY_CHEM_LSD_NIMH: {
            "name": _("LSD NiMH"),
            "full_name": _("Low self-discharge nickel–metal hydride"),
        },
        BATTERY_CHEM_LI_ION: {
            "name": "Li-ion",
            "full_name": "Lithium cobalt oxide",
        },
        BATTERY_CHEM_LTO : {
            "name": "LTO",
            "full_name": "Lithium-titanate",
        },
        BATTERY_CHEM_LIFEPO : {
            "name": "LiFePO4",
            "full_name": "Lithium iron phosphate",
        },
        BATTERY_CHEM_LMO : {
            "name": "LMO",
            "full_name": "Lithium manganese oxide",
        },
    }
)

CYLINDRICAL_CELLS = {
    7540: {'name': '07540', 'height': '40', 'diameter': '7.5'},
    8570: {'name': '08570', 'height': '70', 'diameter': '8.5'},
    10180: {'name': '10180', 'height': '18', 'diameter': '10'},
    10280: {'name': '10280', 'height': '28', 'diameter': '10'},
    10440: {'name': '10440', 'height': '44', 'diameter': '10'},
    10850: {'name': '10850', 'height': '85', 'diameter': '10'},
    13400: {'name': '13400', 'height': '40', 'diameter': '13'},
    14250: {'name': '14250', 'height': '25', 'diameter': '14'},
    14300: {'name': '14300', 'height': '30', 'diameter': '14'},
    14430: {'name': '14430', 'height': '43', 'diameter': '14'},
    14500: {'name': '14500', 'height': '53', 'diameter': '14'},
    14650: {'name': '14650', 'height': '65', 'diameter': '14'},
    15270: {'name': '15270', 'height': '27', 'diameter': '15'},
    16340: {'name': '16340', 'height': '34', 'diameter': '16'},
    16650: {'name': '16650', 'height': '65', 'diameter': '16'},
    17500: {'name': '17500', 'height': '50', 'diameter': '17'},
    17650: {'name': '17650', 'height': '65', 'diameter': '17'},
    17670: {'name': '17670', 'height': '67', 'diameter': '17'},
    18350: {'name': '18350', 'height': '35', 'diameter': '18'},
    18490: {'name': '18490', 'height': '49', 'diameter': '18'},
    18500: {'name': '18500', 'height': '50', 'diameter': '18'},
    18650: {'name': '18650', 'height': '65', 'diameter': '18'},
    20700: {'name': '20700', 'height': '70', 'diameter': '20'},
    21700: {'name': '21700', 'height': '70', 'diameter': '21'},
    25500: {'name': '25500', 'height': '50', 'diameter': '25'},
    26500: {'name': '26500', 'height': '50', 'diameter': '26'},
    26650: {'name': '26650', 'height': '65', 'diameter': '26'},
    26700: {'name': '26700', 'height': '70', 'diameter': '26'},
    26800: {'name': '26800', 'height': '80', 'diameter': '26'},
    32600: {'name': '32600', 'height': '60', 'diameter': '32'},
    32650: {'name': '32650', 'height': '67.7', 'diameter': '32'},
    32700: {'name': '32700', 'height': '70', 'diameter': '32'},
    38120: {'name': '38120', 'height': '120', 'diameter': '38'},
    38140: {'name': '38140', 'height': '140', 'diameter': '38'},
    40152: {'name': '40152', 'height': '152', 'diameter': '40'},
    4680: {'name': '4680', 'height': '80', 'diameter': '46'}
 }


COLOR_RED = 1
COLOR_BLUE = 2
COLOR_GREEN = 3
COLOR_YELLOW = 4
COLOR_PURPLE = 5
COLOR_PINK = 6
COLOR_ORANGE = 7
COLOR_BROWN = 8
COLOR_BLACK = 9
COLOR_WHITE = 10
COLOR_GRAY = 11
COLOR_GOLD = 12
COLOR_SILVER = 13
COLOR_DARK_BLUE = 14
COLOR_LIGHT_BLUE = 15
COLOR_LIGHT_GREEN = 16
COLOR_LIGHT_PURPLE = 17
COLOR_MAROON = 18
COLOR_TEAL = 19
COLOR_TAN = 20
    
COLORS = OrderedDict(
    {
        COLOR_RED: _("Red"),
        COLOR_BLUE: _("Blue"),
        COLOR_GREEN: _("Green"),
        COLOR_YELLOW: _("Yellow"),
        COLOR_PURPLE: _("Purple"),
        COLOR_PINK: _("Pink"),
        COLOR_ORANGE: _("Orange"),
        COLOR_BROWN: _("Brown"),
        COLOR_BLACK: _("Black"),
        COLOR_WHITE: _("White"),
        COLOR_GRAY: _("Gray"),
        COLOR_GOLD: _("Gold"),
        COLOR_SILVER: _("Silver"),
        COLOR_DARK_BLUE: _("Dark Blue"),
        COLOR_LIGHT_BLUE: _("Light Blue"),
        COLOR_LIGHT_GREEN: _("Light Green"),
        COLOR_LIGHT_PURPLE: _("Light Purple"),
        COLOR_MAROON: _("Maroon"),
        COLOR_TEAL: _("Teal"),
        COLOR_TAN: _("Tan"),
    }
)
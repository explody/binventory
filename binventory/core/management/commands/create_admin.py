from django.conf import settings
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password
from django.core.management.base import BaseCommand
from users.models import APIToken


class Command(BaseCommand):
    help = "Initialize the app with default accounts, data, etc"
    label = "username"

    def add_arguments(self, parser):
        super().add_arguments(parser)
        parser.add_argument(
            "-p",
            "--password",
            type=str,
            default="admin",
            help="Admin account password. Defaults to 'admin'",
        )

    def handle(self, **options):
        User = get_user_model()
        password = options["password"]
        try:
            user = User.objects.get(username="admin")
            self.stdout.write("Admin user exists")
        except User.DoesNotExist:
            user = User.objects.create(
                username="admin",
                password=make_password(password),
                first_name="Binventoy",
                last_name="Admin",
                email="admin@binventory.local",
                is_superuser=True,
                is_active=True,
                is_staff=True,
            )

            token = APIToken.objects.create(user=user)

            self.stdout.write("Admin user and API token created")
            self.stdout.write(f"Token: {token.key}")

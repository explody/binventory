user www-data;
worker_processes auto;
pid /run/nginx.pid;

daemon off;
error_log /dev/stdout info;

events {
	worker_connections 768;
	# multi_accept on;
}

http {

    # Set to /dev/stdout if you want webserver access logs, but otherwise it's just very noisy
    access_log /dev/null;

	sendfile on;
	tcp_nopush on;
	types_hash_max_size 2048;

	# include /etc/nginx/mime.types;
	default_type application/octet-stream;

	ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
	ssl_prefer_server_ciphers on;

	gzip on;

    server {

        listen                8000;
        server_name           binv.local;

        resolver 127.0.0.11 ipv6=off;

        client_max_body_size  25m;

        add_header Strict-Transport-Security "max-age=63072000; includeSubDomains; preload";
        add_header X-Frame-Options DENY;
        add_header X-Content-Type-Options nosniff;
        add_header X-XSS-Protection "1; mode=block";

        location /hmr {
            proxy_pass http://127.0.0.1:5174;
            proxy_http_version 1.1;
            proxy_set_header Host $http_host;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }

        location ~ ^/(api|static|admin) {
            proxy_pass http://backend:8001;
            proxy_set_header X-Forwarded-Host $server_name;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_headers_hash_max_size 512;
            add_header P3P 'CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"';
        }

        location / {
            proxy_pass http://127.0.0.1:5173;
            proxy_set_header X-Forwarded-Host $server_name;
            proxy_set_header X-Real-IP $remote_addr;
            proxy_set_header X-Forwarded-Proto $scheme;
            proxy_headers_hash_max_size 512;
            add_header P3P 'CP="ALL DSP COR PSAa PSDa OUR NOR ONL UNI COM NAV"';
        }
    }

}

#!/bin/bash

# add a delay, mostly for the database on very first startup, because
# docker compose start_interval doesn't work yet. Just a few sec is plenty
sleep 3

export APP_ENV="${APP_ENV:=local}"

if [ "$APP_ENV" == "local" ] || [ "$APP_ENV" == "dev" ]; then
    python manage.py makemigrations
    python manage.py migrate
    python manage.py create_admin
    python manage.py collectstatic --no-input
    python manage.py runserver 0:8001
elif [ "$APP_ENV" == "prod" ] || [ "$APP_ENV" == "stage" ]; then
    python manage.py migrate
    python manage.py collectstatic --no-input
    gunicorn -b 0:8001 asgi
fi

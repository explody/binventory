FROM python:3.11-slim as backend
ENV PYTHONUNBUFFERED 1

ENV C_FORCE_ROOT true

# Fix timezone, needs tzdata to be installed
ARG TZ='America/New_York'
ENV TZ ${TZ}

RUN apt-get update
RUN apt-get install --no-install-recommends -y python3-dev gcc musl-dev gettext openssl libssl-dev tzdata htop libpq-dev curl
RUN mkdir -p /app/binventory
COPY binventory /app/binventory
COPY requirements.txt /app/
COPY manage.py /app/
COPY run-backend.sh /app/
RUN cd /app && pip install --no-cache-dir -r requirements.txt
EXPOSE 8000


## 
FROM node:lts-slim as frontend

RUN apt-get update
RUN apt-get install --no-install-recommends -y nginx

RUN mkdir -p /app/frontend
COPY frontend /app/frontend/
COPY run-frontend.sh /app/
RUN cd /app/frontend/ && rm yarn.lock && yarn install
ENV PATH /app/frontend/node_modules/.bin:$PATH

RUN apt-get autoremove
RUN apt-get clean
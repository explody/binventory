#!/bin/bash

export APP_ENV="${APP_ENV:=local}"
PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" &>/dev/null && pwd)"

cd "${PROJECT_DIR}/frontend"

yarn install

if [ "$APP_ENV" == "local" ]; then
    yarn dev
elif [ "$APP_ENV" == "dev" ] || [ "$APP_ENV" == "stage" ]; then
    yarn dev &
    nginx -c /app/nginx.conf
elif [ "$APP_ENV" == "prod" ]; then
    yarn build
    nginx
fi

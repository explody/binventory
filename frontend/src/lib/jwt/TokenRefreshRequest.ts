import { type Token } from './Token'
import { type IAuthTokens } from './IAuthTokens'

export type TokenRefreshRequest = (refreshToken: Token) => Promise<Token | IAuthTokens>

import { type Token } from './Token'

export interface IAuthTokens {
  access: Token
  refresh: Token
}

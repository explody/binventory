import {
  mdiAccountCircle,
  mdiAlertCircle,
  mdiLock,
  mdiMonitor,
  mdiPalette,
  mdiResponsive,
  mdiSquareEditOutline,
  mdiTable,
  mdiTelevisionGuide,
  mdiViewList
} from '@mdi/js'

export default [
  {
    to: '/',
    icon: mdiMonitor,
    label: 'Dashboard',
    href: "/"
  },
  {
    to: '/cells',
    href: '/cells',
    label: 'Cells',
    icon: mdiTable
  },
  {
    to: '/forms',
    href: '/forms',
    label: 'Forms',
    icon: mdiSquareEditOutline
  },
  {
    to: '/profile',
    label: 'Profile',
    href: '/profile',
    icon: mdiAccountCircle
  },
  // {
  //   to: '/login',
  //   label: 'Login',
  //   href: '/login',
  //   icon: mdiLock
  // },
  // {
  //   to: '/error',
  //   label: 'Error',
  //   icon: mdiAlertCircle
  // },
  // {
  //   label: 'Dropdown',
  //   icon: mdiViewList,
  //   menu: [
  //     {
  //       label: 'Item One'
  //     },
  //     {
  //       label: 'Item Two'
  //     }
  //   ]
  // }
]

import { createHead } from "@unhead/vue"
import { createPinia } from 'pinia'
import { createApp } from 'vue'

import App from './App.vue'
import router from './router'

import Vue3EasyDataTable from 'vue3-easy-data-table'
import 'vue3-easy-data-table/dist/style.css'

import './css/main.css'

// Init Pinia
const pinia = createPinia()

//Unhead
const head = createHead()

// Create Vue app
const app = createApp(App)
app.use(router).use(pinia).use(head).mount('#app')
app.component('EasyDataTable', Vue3EasyDataTable);


// @ts-ignore
import { useDarkModeStore } from './stores/darkMode'

const darkModeStore = useDarkModeStore(pinia)

// if (
//   (!localStorage['darkMode'] && window.matchMedia('(prefers-color-scheme: dark)').matches) ||
//   localStorage['darkMode'] === '1'
// ) {
  darkModeStore.set(true)
// }

// Default title tag
const defaultDocumentTitle: string = 'binventory'

// Set document title from route meta
router.afterEach((to) => {
  document.title = to.meta?.title
    ? `${to.meta.title} — ${defaultDocumentTitle}`
    : defaultDocumentTitle
})
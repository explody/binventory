import { clearAuthTokens, isLoggedIn } from '@/lib/jwt';
import { createRouter, createWebHistory, type RouteRecordRaw, type RouteLocationNormalized, type NavigationGuardNext} from 'vue-router';

interface RouteMeta {
  title: string;
  requiresAuth?: boolean;
}

interface RouteConfig {
  meta: RouteMeta;
  path: string;
  name: string;
  component: () => Promise<any>;
  redirect?: (to: RouteLocationNormalized) => RouteLocationNormalized;
}

const routes: Array<RouteRecordRaw> = [
  {
    meta: {
      title: 'Dashboard',
      requiresAuth: true
    },
    path: '/',
    name: 'dashboard',
    component: () => import('@/views/HomeView.vue')
  },
  {
    meta: {
      title: 'Tables',
      requiresAuth: true
    },
    path: '/cells',
    name: 'cells',
    component: () => import('@/views/CellsView.vue')
  },
  {
    meta: {
      title: 'Forms',
      requiresAuth: true
    },
    path: '/forms',
    name: 'forms',
    component: () => import('@/views/FormsView.vue')
  },
  {
    meta: {
      title: 'Profile',
      requiresAuth: true
    },
    path: '/profile',
    name: 'profile',
    component: () => import('@/views/ProfileView.vue')
  },
  {
    meta: {
      title: 'Ui',
      requiresAuth: true
    },
    path: '/ui',
    name: 'ui',
    component: () => import('@/views/UiView.vue')
  },
  {
    meta: {
      title: 'Responsive layout',
      requiresAuth: true
    },
    path: '/responsive',
    name: 'responsive',
    component: () => import('@/views/ResponsiveView.vue')
  },
  {
    meta: {
      title: 'Login'
    },
    path: '/login',
    name: 'login',
    component: () => import('@/views/LoginView.vue')
  },
  {
    meta: {
      title: 'Error'
    },
    path: '/error',
    name: 'error',
    component: () => import('@/views/ErrorView.vue')
  },
  {
    meta: {
      title: 'Logout'
    },
    path: '/logout',
    name: 'logout',
    redirect: (to: RouteLocationNormalized) => {
      clearAuthTokens()
      return { path: '/' }
    }
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition || { top: 0 }
  }
})

router.beforeEach(async (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => {
  if (to.matched.some((record: RouteRecordRaw) => record.meta!.requiresAuth || false)) {
    if (await isLoggedIn()) {
      console.log("isLoggedIn: logged in")
      next()
    } else {
      console.log("isLoggedIn: not logged in")
      next('/login')
    }
  } else {
    next()
  }
})

export default router
import { BinvClient } from './generated/BinvClient';
import { BinventoryHttpClient } from './httpClient';

export const apiClient = new BinvClient(
    { 
        BASE: import.meta.env.VITE_API_URL, 
        WITH_CREDENTIALS: true, 
        // HEADERS: {'Access-Control-Allow-Credentials': 'true'} 
    }, 
    BinventoryHttpClient
)
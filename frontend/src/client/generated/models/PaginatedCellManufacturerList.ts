/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CellManufacturer } from './CellManufacturer';

export type PaginatedCellManufacturerList = {
    count?: number;
    next?: string | null;
    previous?: string | null;
    results?: Array<CellManufacturer>;
};


/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * * `1` - Red
 * * `2` - Blue
 * * `3` - Green
 * * `4` - Yellow
 * * `5` - Purple
 * * `6` - Pink
 * * `7` - Orange
 * * `8` - Brown
 * * `9` - Black
 * * `10` - White
 * * `11` - Gray
 * * `12` - Gold
 * * `13` - Silver
 * * `14` - Dark Blue
 * * `15` - Light Blue
 * * `16` - Light Green
 * * `17` - Light Purple
 * * `18` - Maroon
 * * `19` - Teal
 * * `20` - Tan
 */
export enum ColorEnum {
    '_1' = 1,
    '_2' = 2,
    '_3' = 3,
    '_4' = 4,
    '_5' = 5,
    '_6' = 6,
    '_7' = 7,
    '_8' = 8,
    '_9' = 9,
    '_10' = 10,
    '_11' = 11,
    '_12' = 12,
    '_13' = 13,
    '_14' = 14,
    '_15' = 15,
    '_16' = 16,
    '_17' = 17,
    '_18' = 18,
    '_19' = 19,
    '_20' = 20,
}

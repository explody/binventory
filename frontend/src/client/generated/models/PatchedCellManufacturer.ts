/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type PatchedCellManufacturer = {
    readonly id?: number;
    name?: string;
};


/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ChemistryEnum } from './ChemistryEnum';
import type { FormFactorEnum } from './FormFactorEnum';

export type PatchedCellModel = {
    readonly id?: number;
    name?: string | null;
    model_number?: string | null;
    form_factor?: FormFactorEnum;
    manufacturer?: number;
    chemistry?: ChemistryEnum;
    nominal_capacity?: number;
    minimum_capacity?: number | null;
    nominal_voltage?: number;
    charging_voltage?: number | null;
    cutoff_voltage?: number | null;
    max_charge_current?: number | null;
    max_discharge_current?: number | null;
    peak_discharge_current?: number | null;
    minimum_temperature?: number | null;
    maximum_temperature?: number | null;
    weight?: number | null;
    height?: number | null;
    width?: number | null;
    depth?: number | null;
    diameter?: number | null;
    description?: string;
};


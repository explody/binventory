/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * * `1` - Lead-acid
 * * `2` - RAM
 * * `3` - NiZn
 * * `4` - NiFe
 * * `5` - NiCad
 * * `6` - NiH2
 * * `7` - NiMH
 * * `8` - LSD NiMH
 * * `9` - Li-ion
 * * `10` - LTO
 * * `11` - LiFePO4
 * * `12` - LMO
 */
export enum ChemistryEnum {
    '_1' = 1,
    '_2' = 2,
    '_3' = 3,
    '_4' = 4,
    '_5' = 5,
    '_6' = 6,
    '_7' = 7,
    '_8' = 8,
    '_9' = 9,
    '_10' = 10,
    '_11' = 11,
    '_12' = 12,
}

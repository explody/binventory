/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * * `1` - Cylindrical
 * * `2` - Prismatic
 * * `3` - Pouch
 */
export enum FormFactorEnum {
    '_1' = 1,
    '_2' = 2,
    '_3' = 3,
}

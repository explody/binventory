/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { ColorEnum } from './ColorEnum';

export type PatchedCell = {
    readonly id?: number;
    serial_number?: string;
    model?: number;
    color?: ColorEnum;
};


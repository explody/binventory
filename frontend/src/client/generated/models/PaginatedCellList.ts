/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { Cell } from './Cell';

export type PaginatedCellList = {
    count?: number;
    next?: string | null;
    previous?: string | null;
    results?: Array<Cell>;
};


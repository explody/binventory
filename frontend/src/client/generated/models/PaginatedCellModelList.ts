/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { CellModel } from './CellModel';

export type PaginatedCellModelList = {
    count?: number;
    next?: string | null;
    previous?: string | null;
    results?: Array<CellModel>;
};


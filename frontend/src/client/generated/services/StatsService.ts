/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Stats } from '../models/Stats';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class StatsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * @returns Stats
     * @throws ApiError
     */
    public statsRetrieve(): CancelablePromise<Stats> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/stats',
        });
    }

}

/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { Cell } from '../models/Cell';
import type { CellManufacturer } from '../models/CellManufacturer';
import type { CellModel } from '../models/CellModel';
import type { PaginatedCellList } from '../models/PaginatedCellList';
import type { PaginatedCellManufacturerList } from '../models/PaginatedCellManufacturerList';
import type { PaginatedCellModelList } from '../models/PaginatedCellModelList';
import type { PatchedCell } from '../models/PatchedCell';
import type { PatchedCellManufacturer } from '../models/PatchedCellManufacturer';
import type { PatchedCellModel } from '../models/PatchedCellModel';

import type { CancelablePromise } from '../core/CancelablePromise';
import type { BaseHttpRequest } from '../core/BaseHttpRequest';

export class CellsService {

    constructor(public readonly httpRequest: BaseHttpRequest) {}

    /**
     * Call to super to allow for caching.
     * @param limit Number of results to return per page.
     * @param offset The initial index from which to return the results.
     * @returns PaginatedCellList
     * @throws ApiError
     */
    public cellsList(
        limit?: number,
        offset?: number,
    ): CancelablePromise<PaginatedCellList> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells',
            query: {
                'limit': limit,
                'offset': offset,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param requestBody
     * @returns Cell
     * @throws ApiError
     */
    public cellsCreate(
        requestBody: Cell,
    ): CancelablePromise<Cell> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/v1/cells',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Call to super to allow for caching.
     * @param id
     * @returns Cell
     * @throws ApiError
     */
    public cellsRetrieve(
        id: string,
    ): CancelablePromise<Cell> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id
     * @param requestBody
     * @returns Cell
     * @throws ApiError
     */
    public cellsUpdate(
        id: string,
        requestBody: Cell,
    ): CancelablePromise<Cell> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/v1/cells{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id
     * @param requestBody
     * @returns Cell
     * @throws ApiError
     */
    public cellsPartialUpdate(
        id: string,
        requestBody?: PatchedCell,
    ): CancelablePromise<Cell> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/api/v1/cells{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id
     * @returns void
     * @throws ApiError
     */
    public cellsDestroy(
        id: string,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/v1/cells{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Call to super to allow for caching.
     * @param limit Number of results to return per page.
     * @param offset The initial index from which to return the results.
     * @returns PaginatedCellManufacturerList
     * @throws ApiError
     */
    public cellsManufacturersList(
        limit?: number,
        offset?: number,
    ): CancelablePromise<PaginatedCellManufacturerList> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells/manufacturers',
            query: {
                'limit': limit,
                'offset': offset,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param requestBody
     * @returns CellManufacturer
     * @throws ApiError
     */
    public cellsManufacturersCreate(
        requestBody: CellManufacturer,
    ): CancelablePromise<CellManufacturer> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/v1/cells/manufacturers',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Call to super to allow for caching.
     * @param id A unique integer value identifying this cell manufacturer.
     * @returns CellManufacturer
     * @throws ApiError
     */
    public cellsManufacturersRetrieve(
        id: number,
    ): CancelablePromise<CellManufacturer> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells/manufacturers/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell manufacturer.
     * @param requestBody
     * @returns CellManufacturer
     * @throws ApiError
     */
    public cellsManufacturersUpdate(
        id: number,
        requestBody: CellManufacturer,
    ): CancelablePromise<CellManufacturer> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/v1/cells/manufacturers/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell manufacturer.
     * @param requestBody
     * @returns CellManufacturer
     * @throws ApiError
     */
    public cellsManufacturersPartialUpdate(
        id: number,
        requestBody?: PatchedCellManufacturer,
    ): CancelablePromise<CellManufacturer> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/api/v1/cells/manufacturers/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell manufacturer.
     * @returns void
     * @throws ApiError
     */
    public cellsManufacturersDestroy(
        id: number,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/v1/cells/manufacturers/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Call to super to allow for caching.
     * @param limit Number of results to return per page.
     * @param offset The initial index from which to return the results.
     * @returns PaginatedCellModelList
     * @throws ApiError
     */
    public cellsModelsList(
        limit?: number,
        offset?: number,
    ): CancelablePromise<PaginatedCellModelList> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells/models',
            query: {
                'limit': limit,
                'offset': offset,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param requestBody
     * @returns CellModel
     * @throws ApiError
     */
    public cellsModelsCreate(
        requestBody: CellModel,
    ): CancelablePromise<CellModel> {
        return this.httpRequest.request({
            method: 'POST',
            url: '/api/v1/cells/models',
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Call to super to allow for caching.
     * @param id A unique integer value identifying this cell model.
     * @returns CellModel
     * @throws ApiError
     */
    public cellsModelsRetrieve(
        id: number,
    ): CancelablePromise<CellModel> {
        return this.httpRequest.request({
            method: 'GET',
            url: '/api/v1/cells/models/{id}',
            path: {
                'id': id,
            },
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell model.
     * @param requestBody
     * @returns CellModel
     * @throws ApiError
     */
    public cellsModelsUpdate(
        id: number,
        requestBody: CellModel,
    ): CancelablePromise<CellModel> {
        return this.httpRequest.request({
            method: 'PUT',
            url: '/api/v1/cells/models/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell model.
     * @param requestBody
     * @returns CellModel
     * @throws ApiError
     */
    public cellsModelsPartialUpdate(
        id: number,
        requestBody?: PatchedCellModel,
    ): CancelablePromise<CellModel> {
        return this.httpRequest.request({
            method: 'PATCH',
            url: '/api/v1/cells/models/{id}',
            path: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/json',
        });
    }

    /**
     * Accept either a single object or a list of objects to create.
     * @param id A unique integer value identifying this cell model.
     * @returns void
     * @throws ApiError
     */
    public cellsModelsDestroy(
        id: number,
    ): CancelablePromise<void> {
        return this.httpRequest.request({
            method: 'DELETE',
            url: '/api/v1/cells/models/{id}',
            path: {
                'id': id,
            },
        });
    }

}

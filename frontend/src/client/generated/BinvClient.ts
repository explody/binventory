/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { BaseHttpRequest } from './core/BaseHttpRequest';
import type { OpenAPIConfig } from './core/OpenAPI';
import { AxiosHttpRequest } from './core/AxiosHttpRequest';

import { CellsService } from './services/CellsService';
import { DefaultService } from './services/DefaultService';
import { LoginService } from './services/LoginService';
import { LogoutService } from './services/LogoutService';
import { PasswordService } from './services/PasswordService';
import { StatsService } from './services/StatsService';
import { TokenService } from './services/TokenService';
import { UserService } from './services/UserService';

type HttpRequestConstructor = new (config: OpenAPIConfig) => BaseHttpRequest;

export class BinvClient {

    public readonly cells: CellsService;
    public readonly default: DefaultService;
    public readonly login: LoginService;
    public readonly logout: LogoutService;
    public readonly password: PasswordService;
    public readonly stats: StatsService;
    public readonly token: TokenService;
    public readonly user: UserService;

    public readonly request: BaseHttpRequest;

    constructor(config?: Partial<OpenAPIConfig>, HttpRequest: HttpRequestConstructor = AxiosHttpRequest) {
        this.request = new HttpRequest({
            BASE: config?.BASE ?? '',
            VERSION: config?.VERSION ?? '0.0.0',
            WITH_CREDENTIALS: config?.WITH_CREDENTIALS ?? false,
            CREDENTIALS: config?.CREDENTIALS ?? 'include',
            TOKEN: config?.TOKEN,
            USERNAME: config?.USERNAME,
            PASSWORD: config?.PASSWORD,
            HEADERS: config?.HEADERS,
            ENCODE_PATH: config?.ENCODE_PATH,
        });

        this.cells = new CellsService(this.request);
        this.default = new DefaultService(this.request);
        this.login = new LoginService(this.request);
        this.logout = new LogoutService(this.request);
        this.password = new PasswordService(this.request);
        this.stats = new StatsService(this.request);
        this.token = new TokenService(this.request);
        this.user = new UserService(this.request);
    }
}


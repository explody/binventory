import { applyAuthTokenInterceptor, getBrowserLocalStorage, type IAuthTokens, type TokenRefreshRequest } from '@/lib/jwt';
import axios from 'axios';
import axiosRetry from 'axios-retry';
import { type ApiRequestOptions } from './generated/core/ApiRequestOptions';
import { BaseHttpRequest } from './generated/core/BaseHttpRequest';
import { CancelablePromise } from './generated/core/CancelablePromise';
import type { OpenAPIConfig } from './generated/core/OpenAPI';
import { request as __request } from './generated/core/request';

// 1. Create an axios instance that you wish to apply the interceptor to
// export const axiosInstance = axios.create({ baseURL: import.meta.env.VITE_API_URL })

const requestRefresh: TokenRefreshRequest = async (refreshToken: string): Promise<IAuthTokens | string> => {
  const response = await axios.post(
    `${import.meta.env.VITE_API_URL}/api/v1/token/refresh`, 
    { refresh: refreshToken }
  )
  return response.data.access
}

export class BinventoryHttpClient extends BaseHttpRequest {
  axiosInstance = axios.create();
  

  constructor(config: OpenAPIConfig) {
    super(config);
    
    const getStorage = getBrowserLocalStorage

    axiosRetry(this.axiosInstance);
    applyAuthTokenInterceptor(this.axiosInstance, { requestRefresh, getStorage })
  }

  public override request<T>(options: ApiRequestOptions): CancelablePromise<T> {
    return __request(this.config, options, this.axiosInstance);
  }
}
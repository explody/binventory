const state: {
  visible: boolean;
  isDark: boolean;
  leftDrawerOpen: boolean;
  nextLink: null | string;
  authPanel: string;
} = {
  visible: false,
  isDark: true,
  leftDrawerOpen: false,
  nextLink: null,
  authPanel: "login",
};

const getters: {
  leftDrawerOpen: (s: typeof state) => boolean;
  authModalVisible: (s: typeof state) => boolean;
  getNextLink: (s: typeof state) => null | string;
  getAuthPanel: (s: typeof state) => string;
  isDark: (s: typeof state) => boolean;
} = {
  leftDrawerOpen: (s) => s.leftDrawerOpen,
  authModalVisible: (s) => s.visible,
  getNextLink: (s) => s.nextLink,
  getAuthPanel: (s) => s.authPanel,
  isDark: (s) => s.isDark,
};

const mutations: {
  toggleDarkMode: (s: typeof state) => void;
  setAuthPanel: (s: typeof state, payload: string) => void;
  toggleLoginMenu: (s: typeof state) => void;
  toggleLeftDrawer: (s: typeof state, payload?: { leftDrawerOpen: boolean }) => void;
} = {
  toggleDarkMode: (state) => {
    state.isDark = !state.isDark;
  },
  setAuthPanel: (state, payload) => {
    state.authPanel = payload;
  },
  toggleLoginMenu: (state) => {
    state.visible = !state.visible;
  },
  toggleLeftDrawer: (state, payload) => {
    if (payload) {
      state.leftDrawerOpen = payload.leftDrawerOpen;
      return;
    }
    state.leftDrawerOpen = !state.leftDrawerOpen;
  },
};

const module = {
  state,
  getters,
  mutations,
};

export default module;
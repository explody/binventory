import { defineStore } from 'pinia';
import { computed, type Ref } from 'vue';

interface User {
  name: string;
  email: string;
}

interface MainStore {
  userName: Ref<string>;
  userEmail: Ref<string>;
  userAvatar: Ref<string>;
  isFieldFocusRegistered: Ref<boolean>;
  clients: Ref<any[]>;
  history: Ref<any[]>;
  setUser: (payload: User) => void;
}

export const useMainStore = defineStore('main', (): MainStore => {
  const userName: Ref<string> = ref('');
  const userEmail: Ref<string> = ref('');

  const userAvatar: Ref<string> = computed(() =>
    `https://api.dicebear.com/7.x/avataaars/svg?seed=${userEmail.value.replace(
      /[^a-z0-9]+/gi,
      '-'
    )}`
  );

  const isFieldFocusRegistered: Ref<boolean> = ref(false);

  const clients: Ref<any[]> = ref([]);
  const history: Ref<any[]> = ref([]);

  function setUser(payload: User): void {
    if (payload.name) {
      userName.value = payload.name;
    }
    if (payload.email) {
      userEmail.value = payload.email;
    }
  }

  return {
    userName,
    userEmail,
    userAvatar,
    isFieldFocusRegistered,
    clients,
    history,
    setUser,
  };
});
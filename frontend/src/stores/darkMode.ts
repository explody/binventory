import { defineStore } from 'pinia';
import { ref } from 'vue';

interface DarkModeStore {
  isEnabled: Ref<boolean>;
  set: (payload: boolean | null) => void;
}

export const useDarkModeStore = defineStore('darkMode', () => {
  const isEnabled = ref<boolean>(true);

  function set(payload: boolean | null = null): void {
    isEnabled.value = payload !== null ? payload : !isEnabled.value;

    if (typeof document !== 'undefined') {
      document.body.classList[isEnabled.value ? 'add' : 'remove']('dark-scrollbars');

      document.documentElement.classList[isEnabled.value ? 'add' : 'remove'](
        'dark',
        'dark-scrollbars-compat'
      );
    }

    // You can persist dark mode setting
    if (typeof localStorage !== 'undefined') {
      localStorage.setItem('darkMode', isEnabled.value ? '1' : '0');
    }
  }

  return {
    isEnabled,
    set
  } as DarkModeStore;
});

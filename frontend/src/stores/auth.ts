export const AUTH_REQUEST: string = "REQUEST";
export const AUTH_SUCCESS: string = "SUCCESS";
export const AUTH_ERROR: string = "ERROR";
export const AUTH_LOGOUT: string = "LOGOUT";
export const AUTH_REFRESH: string = "REFRESH";

import { BinvClient } from '@/client/generated';
import { clearAuthTokens, isLoggedIn } from '@/lib/jwt';
import { STORAGE_KEY } from '@/lib/jwt/StorageKey';
import { defineStore } from 'pinia';

const binvClient: BinvClient = new BinvClient({
  BASE: `${import.meta.env.VITE_API_URL}`
});

export const useAuthStore = defineStore('AuthStore', {
  state: (): { authenticated: Promise<boolean>|boolean } => {
    return {
      authenticated: isLoggedIn() || false,
    }
  },
  actions: {
    login: async (username: string, password: string): Promise<string> => {
      try {
        const loginResponse = await binvClient.login.loginCreate({
          username,
          password,
        })
        localStorage.setItem(STORAGE_KEY, JSON.stringify(loginResponse))
        return AUTH_SUCCESS
      } catch (error) {
        console.log("400 bad request is expected from an auth failure")
        console.log(error)
        return AUTH_ERROR
      }
    },
    logout: (): void => {
      binvClient.logout.logoutCreate()
      clearAuthTokens()
    }
  },
})
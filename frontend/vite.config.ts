import { unheadVueComposablesImports } from '@unhead/vue'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { defineConfig } from 'vite'

// https://vitejs.dev/config/
export default defineConfig({
  define: {
    'process.env': process.env
  },
  plugins: [
    vue(),
    AutoImport({ 
      imports: ['vue', 'vue-router', unheadVueComposablesImports, 'pinia'],
       dts: './node_modules/.auto-imports-type/auto-imports.d.ts',
    }),
    Components({ 
      dts: './node_modules/.auto-imports-type/components.d.ts',
     }),
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  optimizeDeps: {
    exclude: ['openapi-client-axios']
  },
  server: {
    hmr: {
      port: 5174, // Listen on this port
      clientPort: 8000, // but tell clients to use this port to go through nginx
      path: '/hmr'
    }
  }
})
